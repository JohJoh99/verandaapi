package eu.veranda.api;

public class APIException extends Exception {
	
	private static final long serialVersionUID = -1218147731055725504L;
	
	private final String message;
	
	public APIException(String message) {
		this.message = message;
	}
	
	@Override
	public String getMessage() {
		return message;
	}

}
