package eu.veranda.api.player;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import eu.veranda.api.APIUtil;

public class DataPlayerStorage {
	
	private static HashMap<UUID, DataPlayer> players = new HashMap<UUID, DataPlayer>();
	private static HashMap<UUID, DataPlayer> team = new HashMap<UUID, DataPlayer>();
	
	public static void registerPlayer(DataPlayer player) { 
		players.put(player.getUniqueId(), player); 
		if(player.hasRank(Rank.BUILDER)) {
			team.put(player.getUniqueId(), player);
		}
	}
	
	public static void unregisterPlayer(UUID playerUUID) {
		players.remove(playerUUID);
		team.remove(playerUUID);
	}
	
	public static boolean isPlayerRegistered(UUID playerUUID) {
		return players.containsKey(playerUUID);
	}
	
	public static DataPlayer getRegisteredPlayer(UUID playerUUID) {
		return players.get(playerUUID);
	}
	
	public static Collection<DataPlayer> getPlayers() {
		return APIUtil.copy(players.values());
	}
	
	public static Collection<DataPlayer> getTeam() {
		return APIUtil.copy(team.values());
	}

}
