package eu.veranda.api.player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import eu.veranda.api.APIException;
import eu.veranda.api.APIUtil;
import eu.veranda.api.VerandaAPI;
import eu.veranda.api.sql.MySQLConnectionUtils;
import eu.veranda.api.sql.MySQLUtil;
import eu.veranda.api.update.DPULManager;

public class FriendUnit {
	
	private final DataPlayer dataPlayer;
	
	private boolean friendRequests;
	private boolean friendJoinMessages;
	private boolean friendJump;
	private List<Integer> friends;
	private List<Integer> sentRequests;
	private List<Integer> gottenRequests;
	
	protected FriendUnit(DataPlayer dataPlayer, Connection con) throws SQLException, APIException {
		this.dataPlayer = dataPlayer;
		reloadData(con);
	}
	
	public void reloadData(Connection con) throws SQLException, APIException {
		ResultSet rs = MySQLUtil.selectAll(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_ID, dataPlayer.getInternalId(), con);
		rs.last();
		if(rs.getRow() == 0) {
			rs.close();
			con.prepareStatement("INSERT INTO `" + VerandaAPI.TABLE_FRIENDS + "` (`" + VerandaAPI.COLUMN_ID + "`, `" + VerandaAPI.COLUMN_PLAYER_UUID + "`) VALUES ('" + dataPlayer.getInternalId() + "', '" + dataPlayer.getUniqueId() + "')").execute();
			rs = con.prepareStatement("SELECT * FROM `" + VerandaAPI.TABLE_FRIENDS + "` WHERE `" + VerandaAPI.COLUMN_ID + "`='" + dataPlayer.getInternalId() + "'").executeQuery();
			rs.last();
		}
		
		friendRequests = rs.getBoolean(VerandaAPI.COLUMN_FRIEND_REQUESTS);
		friendJoinMessages = rs.getBoolean(VerandaAPI.COLUMN_FRIEND_JOIN_MESSAGES);
		friendJump = rs.getBoolean(VerandaAPI.COLUMN_FRIEND_JUMP);
		
		friends = APIUtil.parseIntList(rs.getString(VerandaAPI.COLUMN_FRIENDS));
		if(friends == null) throw new APIException("Failed to parse friend list for player \"" + dataPlayer.getUniqueId() + "\"");
		
		sentRequests = APIUtil.parseIntList(rs.getString(VerandaAPI.COLUMN_SENT_REQUESTS));
		if(sentRequests == null) throw new APIException("Failed to parse sent request list for player \"" + dataPlayer.getUniqueId() + "\"");
		
		gottenRequests = APIUtil.parseIntList(rs.getString(VerandaAPI.COLUMN_GOTTEN_REQUESTS));
		if(gottenRequests == null) throw new APIException("Failed to parse gotten request list for player \"" + dataPlayer.getUniqueId() + "\"");

		rs.close();
	}
	
	public boolean getFriendRequestsEnabled() {
		return friendRequests;
	}
	
	public boolean getFriendJoinMessagesEnabled() {
		return friendJoinMessages;
	}
	
	public boolean getFriendJumpEnabled() {
		return friendJump;
	}
	
	public List<Integer> getFriends() {
		return APIUtil.copy(friends);
	}
	
	public List<Integer> getGottenRequests() {
		return APIUtil.copy(gottenRequests);
	}
	
	public List<Integer> getSentRequests() {
		return APIUtil.copy(sentRequests);
	}
	
	public boolean hasFriend(int friendsInternalId) {
		return friends.contains(friendsInternalId);
	}
	
	public boolean hasSendRequest(int friendsInternalId) {
		return sentRequests.contains(friendsInternalId);
	}
	
	public boolean hasGottenRequest(int friendsInternalId) {
		return gottenRequests.contains(friendsInternalId);
	}
	
	public int getFriendCount() {
		return friends.size();
	}
	
	public boolean canAddFriend() {
		return 75 > friends.size();
	}
	
	public boolean setFriendRequests(boolean friendRequests) {
		if(this.friendRequests == friendRequests) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setFriendRequests(friendRequests, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.setFriendRequests() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean setFriendRequests(boolean friendRequests, Connection con) {
		if(this.friendRequests == friendRequests) return true;
		try {
			updateFriendRequests(friendRequests, con);
			this.friendRequests = friendRequests;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_FRIEND_REQUESTS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.setFriendRequests() occurred an Error");
			return false;
		}
	}
	
	public boolean setFriendJoinMessages(boolean friendJoinMessages) {
		if(this.friendJoinMessages == friendJoinMessages) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setFriendJoinMessages(friendJoinMessages, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.setFriendJoinMessages() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean setFriendJoinMessages(boolean friendJoinMessages, Connection con) {
		if(this.friendJoinMessages == friendJoinMessages) return true;
		try {
			updateFriendJoinMessages(friendJoinMessages, con);
			this.friendJoinMessages = friendJoinMessages;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_FRIEND_JOIN_MESSAGES);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.setFriendJoinMessages() occurred an Error");
			return false;
		}
	}
	
	public boolean setFriendJump(boolean friendJump) {
		if(this.friendJump == friendJump) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setFriendJump(friendJump, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.setFriendJump() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean setFriendJump(boolean friendJump, Connection con) {
		if(this.friendJump == friendJump) return true;
		try {
			updateFriendJump(friendJump, con);
			this.friendJump = friendJump;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_FRIEND_JUMP);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.setFriendJump() occurred an Error");
			return false;
		}
	}
	
	public boolean addFriend(int friendsInternalId) throws IllegalArgumentException {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return addFriend(friendsInternalId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addFriend() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean addFriend(int friendsInternalId, Connection con) throws IllegalArgumentException {
		if(friendsInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			List<Integer> copy = APIUtil.copy(friends);
			copy.add(friendsInternalId);
			updateFriends(copy, con);
			this.friends = copy;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_FRIENDS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addFriend() occurred an Error");
			return false;
		}
	}
	
	public boolean removeFriend(int friendsInternalId) throws IllegalArgumentException {
		if(!friends.contains(friendsInternalId)) return true;
		if(friendsInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return removeFriend(friendsInternalId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeFriend() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean removeFriend(int friendsInternalId, Connection con) throws IllegalArgumentException {
		if(!friends.contains(friendsInternalId)) return true;
		if(friendsInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			List<Integer> copy = APIUtil.copy(friends);
			copy.remove(new Integer(friendsInternalId));
			updateFriends(copy, con);
			this.friends = copy;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_FRIENDS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeFriend() occurred an Error");
			return false;
		}
	}
	
	public boolean addSentRequest(int requestedUsersInternalId) throws IllegalArgumentException {
		if(requestedUsersInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return addSentRequest(requestedUsersInternalId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addSentRequest() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean addSentRequest(int requestedUsersInternalId, Connection con) throws IllegalArgumentException {
		if(requestedUsersInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			List<Integer> copy = APIUtil.copy(sentRequests);
			copy.add(requestedUsersInternalId);
			updateSentRequests(copy, con);
			this.sentRequests = copy;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_SENT_REQUESTS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addSentRequest() occurred an Error");
			return false;
		}
	}
	
	public boolean addGottenRequest(int requestedUsersInternalId) throws IllegalArgumentException {
		if(requestedUsersInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return addGottenRequest(requestedUsersInternalId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addGottenRequest() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean addGottenRequest(int requestedUsersInternalId, Connection con) throws IllegalArgumentException {
		if(requestedUsersInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			List<Integer> copy = APIUtil.copy(gottenRequests);
			copy.add(requestedUsersInternalId);
			updateGottenRequests(copy, con);
			this.gottenRequests = copy;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_GOTTEN_REQUESTS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addGottenRequest() occurred an Error");
			return false;
		}
	}
	
	public boolean removeSentRequest(int sentUsersInternalId) throws IllegalArgumentException {
		if(!sentRequests.contains(sentUsersInternalId)) return true;
		if(sentUsersInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return removeSentRequest(sentUsersInternalId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeSentRequest() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean removeSentRequest(int sentUsersInternalId, Connection con) throws IllegalArgumentException {
		if(!sentRequests.contains(sentUsersInternalId)) return true;
		if(sentUsersInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			List<Integer> copy = APIUtil.copy(sentRequests);
			copy.remove(new Integer(sentUsersInternalId));
			updateSentRequests(copy, con);
			this.sentRequests = copy;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_SENT_REQUESTS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeSentRequest() occurred an Error");
			return false;
		}
	}
	
	public boolean removeGottenRequest(int gottenUsersInternalId) throws IllegalArgumentException {
		if(!gottenRequests.contains(gottenUsersInternalId)) return true;
		if(gottenUsersInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return removeGottenRequest(gottenUsersInternalId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeGottenRequest() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean removeGottenRequest(int gottenUsersInternalId, Connection con) throws IllegalArgumentException {
		if(!gottenRequests.contains(gottenUsersInternalId)) return true;
		if(gottenUsersInternalId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			List<Integer> copy = APIUtil.copy(gottenRequests);
			copy.remove(new Integer(gottenUsersInternalId));
			updateGottenRequests(copy, con);
			this.gottenRequests = copy;
			DPULManager.call(dataPlayer, this.getClass().getName(), VerandaAPI.COLUMN_GOTTEN_REQUESTS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeGottenRequest() occurred an Error");
			return false;
		}
	}
	
	private void updateFriendRequests(boolean friendrequests, Connection con) throws SQLException {
		MySQLUtil.updateBoolean(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_FRIEND_REQUESTS, friendRequests, VerandaAPI.COLUMN_ID, dataPlayer.getInternalId(), con);
	}
	
	private void updateFriendJoinMessages(boolean friendJoinMessages, Connection con) throws SQLException {
		MySQLUtil.updateBoolean(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_FRIEND_JOIN_MESSAGES, friendJoinMessages, VerandaAPI.COLUMN_ID, dataPlayer.getInternalId(), con);
	}
	
	private void updateFriendJump(boolean friendJump, Connection con) throws SQLException {
		MySQLUtil.updateBoolean(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_FRIEND_JUMP, friendJump, VerandaAPI.COLUMN_ID, dataPlayer.getInternalId(), con);
	}
	
	private void updateFriends(List<Integer> friends, Connection con) throws SQLException {
		String strFriends = APIUtil.toString(friends);
		if(friends.size() == 0 || strFriends == null || strFriends.equalsIgnoreCase("") || strFriends.length() == 0) {
			con.prepareStatement("UPDATE `" + VerandaAPI.TABLE_FRIENDS + "` SET `" + VerandaAPI.COLUMN_FRIENDS + "`= NULL WHERE `" + VerandaAPI.COLUMN_ID + "`=" + dataPlayer.getInternalId() + "").execute();
		}
		else {
			MySQLUtil.updateString(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_FRIENDS, strFriends, VerandaAPI.COLUMN_ID, dataPlayer.getInternalId(), con);
		}
	}
	
	private void updateSentRequests(List<Integer> sentRequests, Connection con) throws SQLException {
		String strSentRequests = APIUtil.toString(sentRequests);
		if(sentRequests.size() == 0 || strSentRequests == null || strSentRequests.equalsIgnoreCase("") || strSentRequests.length() == 0) {
			con.prepareStatement("UPDATE `" + VerandaAPI.TABLE_FRIENDS + "` SET `" + VerandaAPI.COLUMN_SENT_REQUESTS + "`= NULL WHERE `" + VerandaAPI.COLUMN_ID + "`=" + dataPlayer.getInternalId() + "").execute();
		}
		else {
			MySQLUtil.updateString(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_SENT_REQUESTS, strSentRequests, VerandaAPI.COLUMN_ID, dataPlayer.getInternalId(), con);
		}
	}
	
	private void updateGottenRequests(List<Integer> gottenRequests, Connection con) throws SQLException {
		String strGottenRequests = APIUtil.toString(gottenRequests);
		if(gottenRequests.size() == 0 || strGottenRequests == null || strGottenRequests.equalsIgnoreCase("") || strGottenRequests.length() == 0) {
			con.prepareStatement("UPDATE `" + VerandaAPI.TABLE_FRIENDS + "` SET `" + VerandaAPI.COLUMN_GOTTEN_REQUESTS + "`= NULL WHERE `" + VerandaAPI.COLUMN_ID + "`=" + dataPlayer.getInternalId() + "").execute();
		}
		else {
			MySQLUtil.updateString(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_GOTTEN_REQUESTS, strGottenRequests, VerandaAPI.COLUMN_ID, dataPlayer.getInternalId(), con);
		}
	}
	
	public static boolean addFriend(int playerId, int friendId) {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return addFriend(playerId, friendId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addFriend() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static boolean addFriend(int playerId, int friendId, Connection con) throws IllegalArgumentException {
		if(playerId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		if(friendId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			ResultSet rs = MySQLUtil.selectAll(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_ID, playerId, con);
			rs.last();
			if(rs.getRow() == 0) return false;
			
			List<Integer> friends = APIUtil.parseIntList(rs.getString(VerandaAPI.COLUMN_GOTTEN_REQUESTS));
			if(friends == null) return false;
			
			friends.add(friendId);
			
			String upload = APIUtil.toString(friends);
			MySQLUtil.updateString(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_FRIENDS, upload, VerandaAPI.COLUMN_ID, playerId, con);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addFriend() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static boolean removeFriend(int playerId, int friendId) {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return removeFriend(playerId, friendId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeFriend() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static boolean removeFriend(int playerId, int friendId, Connection con) {
		if(playerId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		if(friendId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			ResultSet rs = MySQLUtil.selectAll(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_ID, playerId, con);
			rs.last();
			if(rs.getRow() == 0) return false;
			
			List<Integer> friends = APIUtil.parseIntList(rs.getString(VerandaAPI.COLUMN_GOTTEN_REQUESTS));
			if(friends == null) return false;
			
			friends.remove(friendId);
			
			String upload = APIUtil.toString(friends);
			MySQLUtil.updateString(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_FRIENDS, upload, VerandaAPI.COLUMN_ID, playerId, con);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeFriend() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static boolean addGottenRequest(int requesterId, int getterId) throws IllegalArgumentException {
		if(requesterId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		if(getterId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return addGottenRequest(requesterId, getterId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addGottenRequest() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static boolean addGottenRequest(int requesterId, int getterId, Connection con) throws IllegalArgumentException {
		if(requesterId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		if(getterId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			ResultSet rs = MySQLUtil.selectAll(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_ID, getterId, con);
			rs.last();
			if(rs.getRow() == 0) return false;
			
			List<Integer> gottenRequests = APIUtil.parseIntList(rs.getString(VerandaAPI.COLUMN_GOTTEN_REQUESTS));
			if(gottenRequests == null) return false;
			
			gottenRequests.add(requesterId);
			
			String upload = APIUtil.toString(gottenRequests);
			MySQLUtil.updateString(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_GOTTEN_REQUESTS, upload, VerandaAPI.COLUMN_ID, getterId, con);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.addGottenRequest() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static boolean removeSentRequest(int requesterId, int getterId) throws IllegalArgumentException {
		if(requesterId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		if(getterId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return removeSentRequest(requesterId, getterId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeSentRequest() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static boolean removeSentRequest(int requesterId, int getterId, Connection con) throws IllegalArgumentException {
		if(requesterId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		if(getterId < 1) throw new IllegalArgumentException("Overgiven id cannot be < 1");
		try {
			ResultSet rs = MySQLUtil.selectAll(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_ID, requesterId, con);
			rs.last();
			if(rs.getRow() == 0) return false;
			
			List<Integer> sentRequests = APIUtil.parseIntList(rs.getString(VerandaAPI.COLUMN_SENT_REQUESTS));
			if(sentRequests == null) return false;
			
			sentRequests.remove(new Integer(getterId));
			
			String upload = APIUtil.toString(sentRequests);
			MySQLUtil.updateString(VerandaAPI.TABLE_FRIENDS, VerandaAPI.COLUMN_SENT_REQUESTS, upload, VerandaAPI.COLUMN_ID, requesterId, con);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method FriendUnit.removeSentRequest() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}

}
