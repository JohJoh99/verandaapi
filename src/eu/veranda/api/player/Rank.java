package eu.veranda.api.player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.ChatColor;

import eu.veranda.api.VerandaAPI;
import eu.veranda.api.sql.MySQLConnectionUtils;
import eu.veranda.api.sql.MySQLUtil;

public enum Rank {

	ADMIN(100, "Admin", ChatColor.DARK_RED),
	DEVELOPER(80, "Developer", ChatColor.AQUA),
	MODERATOR(60, "Moderator", ChatColor.RED),
	BUILDER(40, "Builder", ChatColor.YELLOW),
	YOUTUBER(30, "Youtuber", ChatColor.DARK_PURPLE),
	PREMIUM(10, "Premium", ChatColor.GOLD),
	USER(0, "User", ChatColor.GRAY);
	
	private int id;
	private String name;
	private ChatColor color;
	
	private Rank(int id, String name, ChatColor color) {
		this.id = id;
		this.name = name;
		this.color = color;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public ChatColor getColor() {
		return this.color;
	}
	
	public boolean isHigherThen(Rank rank) {
		if(rank == null) return false;
		return id > rank.getId();
	}
	
	public static Rank fromId(int id) {
		for(Rank rank : Rank.values()) {
			if(rank.getId() == id) return rank;
		}
		return USER;
	}
	
	public static Rank getRank(UUID playerUuid) throws IllegalArgumentException, SQLException {
		if(playerUuid == null) throw new IllegalArgumentException("UUID cannot be null");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return getRank(playerUuid, con);
		}
		catch(SQLException e) {
			throw e;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static Rank getRank(UUID playerUuid, Connection con) throws IllegalArgumentException, SQLException {
		if(playerUuid == null) throw new IllegalArgumentException("UUID cannot be null");
		ResultSet rs = MySQLUtil.select(VerandaAPI.COLUMN_RANK, VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_PLAYER_UUID, playerUuid.toString(), con);
		rs.last();
		if(rs.getRow() > 0) {
			return Rank.fromId(rs.getInt(VerandaAPI.COLUMN_RANK));
		}
		return null;
	}


}
