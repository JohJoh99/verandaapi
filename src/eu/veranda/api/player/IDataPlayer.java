package eu.veranda.api.player;

import java.sql.Connection;
import java.util.List;

public interface IDataPlayer {
	
	public int getInternalId();
	public Rank getRank();
	public boolean hasRank(Rank minRank);
	public int getXp();
	public int getVerandaLevel();
	public int getXpToNextLevel();
	public int getCoins();
	public boolean hasCoins(int coins);
	public int getLanguage();
	public FriendUnit getFriendUnit();
	public String getTranslation(String key);
	
	public boolean getAutoNickEnabled();
	public boolean getNickAsPremium();
	
	public boolean getSilentHubEnabled();
	public boolean getShowStatsEnabled();
	public boolean getShowOnlyYtAndTeam();
	public boolean getPrivateMessagesEnabled();
	public boolean getTeamMessagesEnabled();
	
	public boolean addXp(int xp) throws IllegalArgumentException;
	public boolean addXp(int xp, Connection con) throws IllegalArgumentException;
	
	public boolean addCoins(int coins) throws IllegalArgumentException;
	public boolean addCoins(int coins, Connection con) throws IllegalArgumentException;
	public boolean subCoins(int coins) throws IllegalArgumentException;
	public boolean subCoins(int coins, Connection con) throws IllegalArgumentException;
	public boolean setCoins(int coins) throws IllegalArgumentException;
	public boolean setCoins(int coins, Connection con) throws IllegalArgumentException;
	
	public boolean setLanguage(int language) throws IllegalArgumentException;
	public boolean setLanguage(int language, Connection con) throws IllegalArgumentException;
	
	public boolean setRank(Rank rank) throws IllegalArgumentException;
	public boolean setRank(Rank rank, Connection con) throws IllegalArgumentException;
	
	public boolean setAutoNickEnabled(boolean flag);
	public boolean setAutoNickEnabled(boolean flag, Connection con);
	public boolean setNickAsPremium(boolean flag);
	public boolean setNickAsPremium(boolean flag, Connection con);
	public boolean setSilentHubEnabled(boolean flag);
	public boolean setSilentHubEnabled(boolean flag, Connection con);
	public boolean setShowStatsEnabled(boolean flag);
	public boolean setShowStatsEnabled(boolean flag, Connection con);
	public boolean setShowOnlyYtAndTeam(boolean flag);
	public boolean setShowOnlyYtAndTeam(boolean flag, Connection con);
	public boolean setPrivateMessagesEnabled(boolean flag);
	public boolean setPrivateMessagesEnabled(boolean flag, Connection con);
	public boolean setTeamMessagesEnabled(boolean flag);
	public boolean setTeamMessagesEnabled(boolean flag, Connection con);

	public boolean hasServerAchievement(int achievementID);
	public boolean awardServerAchievement(int achievementID) throws IllegalArgumentException;
	public boolean awardServerAchievement(int achievementID, Connection con) throws IllegalArgumentException;
	public boolean removeServerAchievement(int achievementID) throws IllegalArgumentException;
	public boolean removeServerAchievement(int achievementID, Connection con) throws IllegalArgumentException;
	public List<Integer> getServerAchievements();

}
