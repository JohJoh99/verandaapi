package eu.veranda.api.player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

import eu.veranda.api.APIException;
import eu.veranda.api.APIUtil;
import eu.veranda.api.VerandaAPI;
import eu.veranda.api.lang.LanguageUtils;
import eu.veranda.api.sql.MySQLConnectionUtils;
import eu.veranda.api.sql.MySQLUtil;
import eu.veranda.api.update.DPULManager;

public class DataPlayer implements IDataPlayer {
	
	private final UUID uniqueId;
	private final String playerName;
	
	private FriendUnit friendUnit;
	
	private int id;
	private Rank rank;
	private int xp;
	private int level;
	private int xpToNextLevel;
	private int coins;
	private int language;
	
	private List<Integer> achievements;

	private boolean autonick;
	private boolean nickAsPremium;
	private boolean silenthub;
	private boolean showOnlyYtAndTeam;
	private boolean allowPartyInvite;
	private boolean showstats;
	private boolean privateMessages;
	private boolean teamMessages;
	
	public DataPlayer(UUID uniqueId, String playerName, Connection con, boolean loadFriends) throws SQLException, IllegalArgumentException, APIException {
		if(uniqueId == null) throw new IllegalArgumentException("Player UUID cannot be null!");
		if(playerName == null) throw new IllegalArgumentException("Player name cannot be null!");
		this.uniqueId = uniqueId;
		this.playerName = playerName;
		if(loadFriends) friendUnit = new FriendUnit(this, con);
		reloadData(con);
	}

	public DataPlayer(UUID uniqueId, String playerName, boolean loadFriends) throws SQLException, IllegalArgumentException, APIException {
		if(uniqueId == null) throw new IllegalArgumentException("Player UUID cannot be null!");
		if(playerName == null) throw new IllegalArgumentException("Player name cannot be null!");
		this.uniqueId = uniqueId;
		this.playerName = playerName;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			reloadData(con);
			if(loadFriends) friendUnit = new FriendUnit(this, con);
		}
		catch(SQLException e) {
			throw e;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public void reloadData(Connection con) throws SQLException {
		ResultSet rs = con.prepareStatement("SELECT * FROM `" + VerandaAPI.TABLE_USERS + "` WHERE `" + VerandaAPI.COLUMN_PLAYER_UUID + "`='" + uniqueId + "'").executeQuery();
		rs.last();
		if(rs.getRow() == 0) {
			rs.close();
			con.prepareStatement("INSERT INTO `" + VerandaAPI.TABLE_USERS + "`(`" + VerandaAPI.COLUMN_PLAYER_UUID + "`, `" + VerandaAPI.COLUMN_PLAYER_NAME + "`) VALUES ('" + uniqueId + "','" + playerName + "')").execute();
			rs = con.prepareStatement("SELECT * FROM `" + VerandaAPI.TABLE_USERS + "` WHERE `" + VerandaAPI.COLUMN_PLAYER_UUID + "`='" + uniqueId + "'").executeQuery();
			rs.last();
		}
		else if(!this.playerName.equals(rs.getString(VerandaAPI.COLUMN_PLAYER_NAME))) {
			MySQLUtil.updateString(VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_PLAYER_NAME, playerName, VerandaAPI.COLUMN_ID, rs.getInt(VerandaAPI.COLUMN_ID), con);
		}
		
		this.id = rs.getInt(VerandaAPI.COLUMN_ID);
		this.rank = Rank.fromId(rs.getInt(VerandaAPI.COLUMN_RANK));
		this.xp = rs.getInt(VerandaAPI.COLUMN_PLAYER_XP);
		this.coins = rs.getInt(VerandaAPI.COLUMN_COINS);
		this.language = rs.getInt(VerandaAPI.COLUMN_LANGUAGE);
		
		this.achievements = APIUtil.parseIntList(rs.getString(VerandaAPI.COLUMN_ACHIEVEMENTS));

		this.autonick = rs.getBoolean(VerandaAPI.COLUMN_AUTONICK);
		this.nickAsPremium = true;
		this.silenthub = rs.getBoolean(VerandaAPI.COLUMN_SILENT_HUB);
		this.showOnlyYtAndTeam = rs.getBoolean(VerandaAPI.COLUMN_SHOW_ONLY_YT_AND_TEAM);
		this.allowPartyInvite = rs.getBoolean(VerandaAPI.COLUMN_ALLOW_PARTY_INVITE);
		this.showstats = rs.getBoolean(VerandaAPI.COLUMN_SHOW_STATS);
		this.privateMessages = rs.getBoolean(VerandaAPI.COLUMN_PRIVATE_MESSAGES);
		this.teamMessages = rs.getBoolean(VerandaAPI.COLUMN_TEAM_MESSAGES);
		
		updateLevel();
		updateXpToNextLevel();
		
		rs.close();
	}
	
	private void updateLevel() {
		int ctx = 0;
		int ctx2 = 0;
		int ctl = 0;
		while(ctx < xp) {
			ctx = getXpOfLevel(ctl);
			ctx2 = getXpOfLevel(ctl + 1);
			if(ctx <= xp && ctx2 > xp) {
				break;
			}
			else {
				ctl++;
			}
		}
		this.level = ctl;
	}
	
	private void updateXpToNextLevel() {
		int nextLevel = this.level + 1;
		int xpOfLevel = getXpOfLevel(nextLevel);
		this.xpToNextLevel = xpOfLevel - this.xp;
	}
	
	public static int getXpOfLevel(int level) {
		if(level < 1) {
			return 0;
		}
		else if(level <= 15) {
			return level * level + 6 * level;
		}
		else if(level <= 30) {
			return (int) Math.floor(2.5 * (level * level) - 40.5 * level + 360);
		}
		else {
			return (int) Math.floor(4.5 * (level * level) - 162.5 * level + 2220);
		}
	}
	
	public UUID getUniqueId() {
		return uniqueId;
	}
	
	public String getName() {
		return playerName;
	}

	public int getInternalId() {
		return this.id;
	}

	public Rank getRank() {
		return this.rank;
	}
	
	public int getXp() {
		return this.xp;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public int getXpToNextLevel() {
		return this.xpToNextLevel;
	}
	
	public int getCoins(){
		return this.coins;
	}
	
	public boolean hasRank(Rank minRank){
		return this.rank.getId() >= minRank.getId();
	}
	
	public int getLanguage() {
		return language;
	}
	
	public List<Integer> getServerAchievements() {
		return APIUtil.copy(achievements);
	}
	
	public boolean hasServerAchievement(int achievementId) {
		return achievements.contains(achievementId);
	}
	
	public boolean getAutoNickEnabled() {
		return autonick;
	}
	
	public boolean getNickAsPremium() {
		return nickAsPremium;
	}
	
	public boolean getSilentHubEnabled() {
		return silenthub;
	}
	
	public boolean getShowOnlyYtAndTeam() {
		return showOnlyYtAndTeam;
	}
	
	public boolean getAllowPartyInvite() {
		return allowPartyInvite;
	}
	
	public boolean getShowStatsEnabled() {
		return showstats;
	}
	
	public boolean getPrivateMessagesEnabled() {
		return privateMessages;
	}
	
	public boolean getTeamMessagesEnabled() {
		return teamMessages;
	}
	
	public FriendUnit getFriendUnit() {
		return friendUnit;
	}
	
	public String getTranslation(String key) throws IllegalStateException {
		return LanguageUtils.getTranslation(key, getLanguage());
	}
	
	public boolean addXp(int xp) throws IllegalArgumentException {
		if(xp < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return addXp(xp, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.addXp() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean addXp(int xp, Connection con) throws IllegalArgumentException {
		if(xp < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		try {
			updateXp(this.xp + xp, con);
			this.xp += xp;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_PLAYER_XP);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.addXp() occurred an Error");
			return false;
		}
	}
	
	public boolean addCoins(int coins) throws IllegalArgumentException {
		if(coins < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return addCoins(coins, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.addCoins() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean addCoins(int coins, Connection con) throws IllegalArgumentException {
		if(coins < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		try {
			updateCoins(this.coins + coins, con);
			this.coins += coins;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_COINS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.addCoins() occurred an Error");
			return false;
		}
	}

	public boolean subCoins(int coins) throws IllegalArgumentException {
		if(this.coins < 0) throw new IllegalArgumentException("Player has not enough Coins!");
		if(coins < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		if(coins < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return subCoins(coins, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.subCoins() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean subCoins(int coins, Connection con) throws IllegalArgumentException {
		if(this.coins < 0) throw new IllegalArgumentException("Player has not enough Coins!");
		if(coins < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		if(coins < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		try {
			updateCoins(this.coins - coins, con);
			this.coins -= coins;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_COINS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.subCoins() occurred an Error");
			return false;
		}
	}

	public boolean setCoins(int coins) throws IllegalArgumentException {
		if(coins < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		if(this.coins == coins) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setCoins(coins, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setCoins() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean setCoins(int coins, Connection con) throws IllegalArgumentException {
		if(coins < 0) throw new IllegalArgumentException("Overgiven coins cannot be < 0");
		if(this.coins == coins) return true;
		try {
			updateCoins(coins, con);
			this.coins = coins;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_COINS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.subCoins() occurred an Error");
			return false;
		}
	}
	
	public boolean hasCoins(int coins) {
		return this.coins >= coins;
	}
	
	public boolean setLanguage(int language) throws IllegalArgumentException {
		if(language < 0) throw new IllegalArgumentException("Language ID cannot be < 0");
		if(this.language == language) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setLanguage(language, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setLanguage() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean setLanguage(int language, Connection con) throws IllegalArgumentException {
		if(language < 0) throw new IllegalArgumentException("Language ID cannot be < 0");
		if(this.language == language) return true;
		try {
			updateLanguage(language, con);
			this.language = language;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_LANGUAGE);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setLanguage() occurred an Error");
			return false;
		}
	}

	public boolean setRank(Rank rank) throws IllegalArgumentException {
		if(rank == null) throw new IllegalArgumentException("Rank cannot be null");
		if(this.rank == rank) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setRank(rank, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setRank() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}

	public boolean setRank(Rank rank, Connection con) throws IllegalArgumentException {
		if(rank == null) throw new IllegalArgumentException("Rank cannot be null");
		if(this.rank == rank) return true;
		try {
			updateRank(rank, con);
			this.rank = rank;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_RANK);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setRank() occurred an Error");
			return false;
		}
	}
	
	public boolean awardServerAchievement(int achievementId) {
		if(achievementId < 0) throw new IllegalArgumentException("Achievement ID cannot be < 0");
		if(this.achievements.contains(achievementId)) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return awardServerAchievement(achievementId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.addAchievement() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean awardServerAchievement(int achievementId, Connection con) {
		if(achievementId < 0) throw new IllegalArgumentException("Achievement ID cannot be < 0");
		if(this.achievements.contains(achievementId)) return true;
		try {
			List<Integer> copy = APIUtil.copy(achievements);
			copy.add(achievementId);
			updateAchievements(copy, con);
			this.achievements = copy;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_ACHIEVEMENTS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.addAchievement() occurred an Error");
			return false;
		}
	}
	
	public boolean removeServerAchievement(int achievementId) {
		if(achievementId < 0) throw new IllegalArgumentException("Achievement ID cannot be < 0");
		if(!this.achievements.contains(achievementId)) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return removeServerAchievement(achievementId, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.removeAchievement() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean removeServerAchievement(int achievementId, Connection con) {
		if(achievementId < 0) throw new IllegalArgumentException("Achievement ID cannot be < 0");
		if(!this.achievements.contains(achievementId)) return true;
		try {
			List<Integer> copy = APIUtil.copy(achievements);
			copy.remove(achievementId);
			updateAchievements(copy, con);
			this.achievements = copy;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_ACHIEVEMENTS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.removeAchievement() occurred an Error");
			return false;
		}
	}
	
	public boolean setAutoNickEnabled(boolean flag) {
		if(this.autonick == flag) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setAutoNickEnabled(flag, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setAutoNickEnabled() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}

	public boolean setAutoNickEnabled(boolean flag, Connection con) {
		if(this.autonick == flag) return true;
		try {
			updateBoolean(VerandaAPI.COLUMN_AUTONICK, flag, con);
			this.autonick = flag;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_AUTONICK);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setAutoNickEnabled() occurred an Error");
			return false;
		}
	}

	public boolean setSilentHubEnabled(boolean flag) {
		if(this.silenthub == flag) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setSilentHubEnabled(flag, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setSilentHubEnabled() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}

	public boolean setSilentHubEnabled(boolean flag, Connection con) {
		if(this.silenthub == flag) return true;
		try {
			updateBoolean(VerandaAPI.COLUMN_SILENT_HUB, flag, con);
			this.silenthub = flag;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_SILENT_HUB);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setSilentHubEnabled() occurred an Error");
			return false;
		}
	}
	
	public boolean setShowOnlyYtAndTeam(boolean flag) {
		if(this.showOnlyYtAndTeam == flag) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setShowOnlyYtAndTeam(flag, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setShowOnlyYtAndTeam() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean setShowOnlyYtAndTeam(boolean flag, Connection con) {
		if(this.showOnlyYtAndTeam == flag) return true;
		try {
			updateBoolean(VerandaAPI.COLUMN_SHOW_ONLY_YT_AND_TEAM, flag, con);
			this.showOnlyYtAndTeam = flag;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_SHOW_ONLY_YT_AND_TEAM);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setShowOnlyYtAndTeam() occurred an Error");
			return false;
		}
	}
	
	public boolean setAllowPartyInvite(boolean flag) {
		if(this.allowPartyInvite == flag) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setAllowPartyInvite(flag, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setAllowPartyInvite() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean setAllowPartyInvite(boolean flag, Connection con) {
		if(this.allowPartyInvite == flag) return true;
		try {
			updateBoolean(VerandaAPI.COLUMN_ALLOW_PARTY_INVITE, flag, con);
			this.allowPartyInvite = flag;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_ALLOW_PARTY_INVITE);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setAllowPartyInvite() occurred an Error");
			return false;
		}
	}

	public boolean setShowStatsEnabled(boolean flag) {
		if(this.showstats == flag) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setShowStatsEnabled(flag, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setShowStatsEnabled() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}

	public boolean setShowStatsEnabled(boolean flag, Connection con) {
		if(this.showstats == flag) return true;
		try {
			updateBoolean(VerandaAPI.COLUMN_SHOW_STATS, flag, con);
			this.showstats = flag;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_SHOW_STATS);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setShowStatsEnabled() occurred an Error");
			return false;
		}
	}

	public boolean setPrivateMessagesEnabled(boolean flag) {
		if(this.privateMessages == flag) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setPrivateMessagesEnabled(flag, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setPrivateMessagesEnabled() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}

	public boolean setPrivateMessagesEnabled(boolean flag, Connection con) {
		if(this.privateMessages == flag) return true;
		try {
			updateBoolean(VerandaAPI.COLUMN_PRIVATE_MESSAGES, flag, con);
			this.privateMessages = flag;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_PRIVATE_MESSAGES);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setPrivateMessagesEnabled() occurred an Error");
			return false;
		}
	}

	public boolean setTeamMessagesEnabled(boolean flag) {
		if(this.teamMessages == flag) return true;
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return setTeamMessagesEnabled(flag, con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setTeamMessagesEnabled() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}

	public boolean setTeamMessagesEnabled(boolean flag, Connection con) {
		if(this.teamMessages == flag) return true;
		try {
			updateBoolean(VerandaAPI.COLUMN_TEAM_MESSAGES, flag, con);
			this.teamMessages = flag;
			DPULManager.call(this, this.getClass().getName(), VerandaAPI.COLUMN_TEAM_MESSAGES);
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method DataPlayer.setTeamMessagesEnabled() occurred an Error");
			return false;
		}
	}
	
	private void updateXp(int xp, Connection con) throws SQLException {
		MySQLUtil.updateInt(VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_PLAYER_XP, xp, VerandaAPI.COLUMN_ID, this.id, con);
	}
	
	private void updateCoins(int coins, Connection con) throws SQLException {
		MySQLUtil.updateInt(VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_COINS, coins, VerandaAPI.COLUMN_ID, this.id, con);
	}
	
	private void updateLanguage(int language, Connection con) throws SQLException {
		MySQLUtil.updateInt(VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_LANGUAGE, language, VerandaAPI.COLUMN_ID, this.id, con);
	}
	
	private void updateRank(Rank rank, Connection con) throws SQLException {
		MySQLUtil.updateInt(VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_RANK, rank.getId(), VerandaAPI.COLUMN_ID, this.id, con);
	}
	
	private void updateAchievements(List<Integer> achievements, Connection con) throws SQLException {
		String strAchievements = APIUtil.toString(achievements);
		MySQLUtil.updateString(VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_ACHIEVEMENTS, strAchievements, VerandaAPI.COLUMN_ID, this.id, con);
	}
	
	private void updateBoolean(String colName, boolean value, Connection con) throws SQLException {
		MySQLUtil.updateBoolean(VerandaAPI.TABLE_USERS, colName, value, VerandaAPI.COLUMN_ID, this.id, con);
	}

	@Override
	public int getVerandaLevel() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean setNickAsPremium(boolean flag, Connection con) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setNickAsPremium(boolean flag) {
		// TODO Auto-generated method stub
		return false;
	}

}
