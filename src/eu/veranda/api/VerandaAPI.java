package eu.veranda.api;

import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.ChatColor;

public class VerandaAPI {
	
	public static final String VERANDA_PREFIX = ChatColor.GREEN + "Veranda" + ChatColor.DARK_GRAY + "\u00BB " + ChatColor.WHITE;
	public static final String API_VERSION = "1.0.0";
	public static final String API_PREFIX = "[VerandaAPI v" + API_VERSION + "] ";
	public static final String SERVER_NAME;

	public static final String TABLE_USERS = "users";
	public static final String TABLE_MAPS = "maps";
	public static final String TABLE_MOTD = "motd";
	public static final String TABLE_LANG = "lang";
	public static final String TABLE_SIGNS_LOBBY = "signs_lobby";
	public static final String TABLE_FRIENDS = "friends";
	public static final String TABLE_DEFENDERS_INVENTORYS = "defenders_inventorys";
	public static final String TABLE_BUILD_MAPS = "build_maps";

	public static final String COLUMN_ID = "id";
	public static final String COLUMN_PLAYER_UUID = "player_uuid";
	public static final String COLUMN_PLAYER_NAME = "player_name";
	public static final String COLUMN_COINS = "coins";
	public static final String COLUMN_LANGUAGE = "language";
	public static final String COLUMN_RANK = "rank";
	public static final String COLUMN_PLAYER_XP = "player_xp";
	public static final String COLUMN_ONLINE = "online";
	public static final String COLUMN_CURRENT_SERVER = "current_server";
	public static final String COLUMN_FIRST_LOGIN = "first_login";
	public static final String COLUMN_LAST_LOGIN = "last_login";
	public static final String COLUMN_LAST_LOGOUT = "last_logout";
	public static final String COLUMN_ACHIEVEMENTS = "achievements";
	public static final String COLUMN_AUTONICK = "autonick";
	public static final String COLUMN_SILENT_HUB = "silent_hub";
	public static final String COLUMN_SHOW_ONLY_YT_AND_TEAM = "show_only_yt_and_team";
	public static final String COLUMN_ALLOW_PARTY_INVITE = "allow_party_invite";
	public static final String COLUMN_SHOW_STATS = "show_stats";
	public static final String COLUMN_PRIVATE_MESSAGES = "private_messages";
	public static final String COLUMN_TEAM_MESSAGES = "team_messages";
	public static final String COLUMN_KEY = "key";
	public static final String COLUMN_GERMAN = "german";
	public static final String COLUMN_ENGLISH = "english";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_GAMEMODE = "gamemode";
	public static final String COLUMN_MAP_TYPE = "map_type";
	public static final String COLUMN_AUTHOR = "author";
	public static final String COLUMN_DESCRIPTION = "description";
	public static final String COLUMN_LOCATION = "location";
	public static final String COLUMN_EXTRA_TAGS = "extra_tags";
	public static final String COLUMN_TIMES_PLAYED = "times_played";
	public static final String COLUMN_VALUE = "value";
	public static final String COLUMN_SERVER_NAME = "server_name";
	public static final String COLUMN_X = "x";
	public static final String COLUMN_Y = "y";
	public static final String COLUMN_Z = "z";
	public static final String COLUMN_BLOCK_FACE = "block_face";
	public static final String COLUMN_FRIEND_REQUESTS = "friend_requests";
	public static final String COLUMN_FRIEND_JOIN_MESSAGES = "friend_join_messages";
	public static final String COLUMN_FRIEND_JUMP = "friend_jump";
	public static final String COLUMN_FRIENDS = "friends";
	public static final String COLUMN_SENT_REQUESTS = "sent_requests";
	public static final String COLUMN_GOTTEN_REQUESTS = "gotten_requests";
	public static final String COLUMN_MAIN_INVENTORY = "main_inventory";
	public static final String COLUMN_STORAGE_INVENTORY = "storage_inventory";
	public static final String COLUMN_STORAGE_INVENTORY_SIZE = "storage_inventory_size";
	public static final String COLUMN_MAP_NAME = "map_name";
	public static final String COLUMN_MAP_SPAWN_X = "map_spawn_x";
	public static final String COLUMN_MAP_SPAWN_Y = "map_spawn_y";
	public static final String COLUMN_MAP_SPAWN_Z = "map_spawn_z";
	public static final String COLUMN_MAP_SPAWN_YAW = "map_spawn_yaw";
	public static final String COLUMN_MAP_SPAWN_PITCH = "map_spawn_pitch";
	public static final String COLUMN_MAP_LOADED = "map_loaded";
	public static final String COLUMN_MAP_GENERATOR = "map_generator";
	public static final String COLUMN_MAP_GAMEMODE = "map_gamemode";
	public static final String COLUMN_MAP_AUTHOR = "map_author";
	public static final String COLUMN_MAP_DESCRIPTION = "map_description";
	public static final String COLUMN_MAP_EXTRA_TAGS = "map_extra_tags";
	
	private static Logger systemLogger;
	
	static {
		if(isBukkit()) {
			try {
				Class<?> bukkit = Class.forName("org.bukkit.Bukkit");
				Method getLogger = bukkit.getMethod("getLogger");
				Object oLogger = getLogger.invoke(null);
				if(oLogger != null) {
					systemLogger = (Logger) oLogger;
					log(Level.INFO, API_PREFIX, "Logger initialized!");
				}
			}
			catch(Exception e) {
				logMinimal(Level.SEVERE, API_PREFIX, e, "Failed to get logger!");
			}
		}
		else if(isBungeeCord()) {
			try {
				Class<?> bungeeCord = Class.forName("net.md_5.bungee.BungeeCord");
				Method getInstance = bungeeCord.getMethod("getInstance");
				Object instance = getInstance.invoke(null);
				Method getLogger = bungeeCord.getMethod("getLogger");
				Object oLogger = getLogger.invoke(instance);
				if(oLogger != null) {
					systemLogger = (Logger) oLogger;
					log(Level.INFO, API_PREFIX, "Logger initialized!");
				}
			}
			catch(Exception e) {
				logMinimal(Level.SEVERE, API_PREFIX, e, "Failed to get logger!");
			}
		}
		
		log(Level.INFO, API_PREFIX, "Server is running on \"" + getSoftware() + "\"");
		if(System.getProperty("server-name") == null) {
			SERVER_NAME = "Unknown";
			log(Level.INFO, API_PREFIX, "The Server-Name is unknown!");
		}
		else {
			SERVER_NAME = System.getProperty("server-name");
			log(Level.INFO, API_PREFIX, "Server Name: " + SERVER_NAME);
		}
	}
	
	public static void init() {}
	
	public static boolean isBukkit() {
		try {
			Class.forName("org.bukkit.Bukkit");
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isSpigot() {
		try {
			Class.forName("org.spigotmc.WatchdogThread");
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	public static boolean isBungeeCord() {
		try {
			Class.forName("net.md_5.bungee.BungeeCord");
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}
	
	public static String getSoftware() {
		if(isSpigot()) {
			return "SPIGOT";
		}
		else if(isBukkit()) {
			return "BUKKIT";
		}
		else if(isBungeeCord()) {
			return "BUNGEE_CORD";
		}
		else {
			return "UNKNOWN";
		}
	}
	
	/**
	 * Logs a message.
	 * If the level is set to {@code SEVERE} then the error will be reported via Pushbullet
	 * If no report is needed use {@code logNoReport()}
	 * 
	 * @param level The level of this message
	 * @param prefix The prefix of the plugin which calls this method
	 * @param message The message
	 * @return {@code true} because voids are buggy if many logs are called in a row
	 */
	public static boolean log(Level level, String prefix, String message) {
		logNoReport(level, prefix, message);
		
		if(level == Level.SEVERE) {
			// TODO
//			try {
//				PushClient.send("SEVERE on " + VerandaAPI.SERVER_NAME, prefix + ": " + message);
//			}
//			catch (IOException e) {}
		}
		return true;
	}
	
	public static boolean logMinimal(Level level, String prefix, Throwable t) {
		return log(level, prefix, t.getClass().getName() + ": " + t.getMessage().replace("\n", " - "));
	}
	
	public static boolean logMinimal(Level level, String prefix, Throwable t, String message) {
		if(t == null) return false;
		if(t.getMessage() != null) {
			return log(level, prefix, message + " (" + t.getClass().getName() + ": " + t.getMessage().replace("\n", " - ") + ")");
		}
		else {
			return log(level, prefix, message + " (" + t.getClass().getName() + ": null)");
		}
	}
	
	public static boolean log(Throwable t, String prefix, Thread executingThread) {
		return log("Thread '" + executingThread.getName() + "' occurred an Exception", prefix, t, executingThread);
	}
	
	public static boolean log(String msg, String prefix, Throwable t, Thread executingThread) {
		logNoReport(Level.SEVERE, prefix, msg + ": " + t.getClass().getName() + ": " + t.getMessage());
		for(StackTraceElement ste : t.getStackTrace()) {
			logNoReport(Level.SEVERE, prefix, "	at " + ste.toString());
		}
		return true;
	}
	
	public static boolean logNoReport(Level level, String prefix, String message) {
		if(systemLogger != null) {
			systemLogger.log(level, prefix + " " + message);
		}
		else {
			System.out.println("[" + level.toString() + "] " + prefix + ": " + message);
		}
		return true;
	}

}
