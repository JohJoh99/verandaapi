package eu.veranda.api;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.logging.Level;

import eu.veranda.api.sql.MySQLConnectionUtils;
import eu.veranda.api.sql.MySQLUtil;

public class LoggerUtil {
	
	public static UUID getUUID(String name) throws SQLException {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return getUUID(name, con);
		}
		catch(SQLException e) {
			throw e;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static UUID getUUID(String name, Connection con) throws SQLException {
		ResultSet rs = MySQLUtil.select(VerandaAPI.COLUMN_PLAYER_UUID, VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_PLAYER_NAME, name, con);
		rs.last();
		if(rs.getRow() == 0) return null;
		try {
			return UUID.fromString(rs.getString(VerandaAPI.COLUMN_PLAYER_UUID));
		}
		catch(IllegalArgumentException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method LoggerUtil.getUUIDfromName() occurred an Error");
			return null;
		}
	}
	
	public static String getName(UUID uuid) throws SQLException {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return getName(uuid, con);
		}
		catch(SQLException e) {
			throw e;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static String getName(UUID uuid, Connection con) throws SQLException {
		ResultSet rs = MySQLUtil.select(VerandaAPI.COLUMN_PLAYER_NAME, VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_PLAYER_UUID, uuid.toString(), con);
		rs.last();
		if(rs.getRow() == 0) return null;
		return rs.getString(VerandaAPI.COLUMN_PLAYER_NAME);
	}
	
	public static String getName(int internalId) throws SQLException {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return getName(internalId, con);
		}
		catch(SQLException e) {
			throw e;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static String getName(int internalId, Connection con) throws SQLException {
		ResultSet rs = MySQLUtil.select(VerandaAPI.COLUMN_PLAYER_NAME, VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_ID, internalId, con);
		rs.last();
		if(rs.getRow() == 0) return null;
		return rs.getString(VerandaAPI.COLUMN_PLAYER_NAME);
	}
	
	public static int getInternalId(String name) throws SQLException {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return getInternalId(name, con);
		}
		catch(SQLException e) {
			throw e;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public static int getInternalId(String name, Connection con) throws SQLException {
		ResultSet rs = MySQLUtil.select(VerandaAPI.COLUMN_ID, VerandaAPI.TABLE_USERS, VerandaAPI.COLUMN_PLAYER_NAME, name, con);
		rs.last();
		if(rs.getRow() == 0) return -1;
		return rs.getInt(VerandaAPI.COLUMN_ID);
	}

}
