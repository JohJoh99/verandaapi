package eu.veranda.api.lang;

import java.util.HashMap;
import java.util.Map.Entry;

public class StringReplacer {
	
	private HashMap<String, String> replace = new HashMap<String, String>();
	
	public StringReplacer(String... replace) {
		String key = null;
		for(String s : replace) {
			if(key == null) {
				key = s;
			}
			else {
				this.replace.put(key, s);
				key = null;
			}
		}
	}
	
	public void addReplace(String key, String value) {
		replace.put(key, value);
	}
	
	public String replace(String str) {
		for(Entry<String, String> e : replace.entrySet()) {
			str = str.replace(e.getKey(), e.getValue());
		}
		return str;
	}

}
