package eu.veranda.api.lang;

import java.util.HashMap;

import org.bukkit.ChatColor;

public class APILangKeys {
	
	public static final String SERVER_RESTART = "server.restart";
	public static final String COMMAND_NO_PERMISSION = "command.no.permission";
	public static final String COMMAND_NO_EXECUTION_POSSIBLE = "command.no.execution.possible";
	
	private static HashMap<String, String> defaults = new HashMap<String, String>();
	
	static {
		defaults.put(SERVER_RESTART, ChatColor.RED + "" + ChatColor.BOLD + "Der Server wird in 15 Sekunden neu gestartet!");
		defaults.put(COMMAND_NO_PERMISSION, ChatColor.RED + "Du hast nicht gen�gend Rechte um diesen Befehl zu nutzen");
		defaults.put(COMMAND_NO_EXECUTION_POSSIBLE, ChatColor.RED + "Du kannst diesen Befehl nicht ausf�hren");
	}
	
	private APILangKeys() {}
	
	public static String getDefault(String key) {
		return defaults.containsKey(key) ? defaults.get(key) : key;
	}
	
	public static HashMap<String, String> getDefaults() {
		return defaults;
	}

}
