package eu.veranda.api.lang;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import eu.veranda.api.VerandaAPI;

public class LanguageUtils {
	
	private static boolean initialized = false;
	private static HashMap<String, String> languageDefaults = new HashMap<String, String>();
	private static HashMap<String, List<String>> languageKeys = new HashMap<String, List<String>>();
	
	private LanguageUtils() {}
	
	public static void initialize(Connection con) throws SQLException {
		initialized = true;
		try {
			addTranslations(VerandaAPI.TABLE_LANG, APILangKeys.getDefaults().entrySet(), con);
		}
		catch(SQLException e) {
			initialized = false;
			throw e;
		}
	}
	
	public static void addTranslations(String table, Set<Entry<String, String>> defaults, Connection con) throws IllegalStateException, SQLException {
		if(!initialized) throw new IllegalStateException("LanguageUtils are not initialized!");
		ResultSet checkTable = con.prepareStatement("SHOW TABLES LIKE '" + table + "'").executeQuery();
		checkTable.last();
		if(checkTable.getRow() == 0) {
			con.prepareStatement("CREATE TABLE `" + table + "` (`" + VerandaAPI.COLUMN_KEY + "` varchar(255) NOT NULL, `" + VerandaAPI.COLUMN_GERMAN + "` varchar(255) NOT NULL, `" + VerandaAPI.COLUMN_ENGLISH + "` varchar(255) NULL, PRIMARY KEY (`" + VerandaAPI.COLUMN_KEY + "`)) ENGINE=InnoDB DEFAULT CHARSET=utf8").execute();
			for(Entry<String, String> e : defaults) {
				languageDefaults.put(e.getKey(), e.getValue());
				con.prepareStatement("INSERT INTO `" + table + "` (`" + VerandaAPI.COLUMN_KEY + "`, `" + VerandaAPI.COLUMN_GERMAN + "`) VALUES ('" + e.getKey() + "','" + e.getValue() + "')").execute();
			}
		}
		else {
			for(Entry<String, String> e : defaults) {
				languageDefaults.put(e.getKey(), e.getValue());
			}
		}
		
		ResultSet rs = con.prepareStatement("SELECT * FROM `" + table + "`").executeQuery();
		while(rs.next()) {
			languageKeys.put(rs.getString(VerandaAPI.COLUMN_KEY), Arrays.asList(rs.getString(VerandaAPI.COLUMN_GERMAN), rs.getString(VerandaAPI.COLUMN_ENGLISH)));
		}
	}
	
	public static String getTranslation(String key, int language) throws IllegalStateException {
		if(!initialized) throw new IllegalStateException("LanguageUtils are not initialized!");
		if(languageKeys.containsKey(key)) {
			List<String> values = languageKeys.get(key);
			if(values.size() > language) {
				return values.get(language) != null ? values.get(language) : key;
			}
			else {
				return values.get(0);
			}
		}
		else if(languageDefaults.containsKey(key)) {
			return languageDefaults.get(key);
		}
		return key;
	}

}
