package eu.veranda.api.update;

import java.util.HashSet;

import eu.veranda.api.player.DataPlayer;

public class DPULManager {
	
	private static HashSet<DataPlayerUpdateListener> listeners = new HashSet<DataPlayerUpdateListener>();
	
	public static void addListener(DataPlayerUpdateListener listener) {
		listeners.add(listener);
	}
	
	public static void call(DataPlayer dataPlayer, String section, String updatedColumn) {
		for(DataPlayerUpdateListener l : listeners) {
			l.onDataPlayerUpdate(dataPlayer, section, updatedColumn);
		}
	}
	
	public static void callLocal(DataPlayer dataPlayer, String section, String updatedColumn) {
		for(DataPlayerUpdateListener l : listeners) {
			l.onLocalDataPlayerUpdate(dataPlayer, section, updatedColumn);
		}
	}
	
	public static void call(String playerName, String section, String updatedColumn) {
		for(DataPlayerUpdateListener l : listeners) {
			l.onPlayerUpdate(playerName, section, updatedColumn);
		}
	}

}
