package eu.veranda.api.update;

import eu.veranda.api.player.DataPlayer;

public interface DataPlayerUpdateListener {
	
	public void onDataPlayerUpdate(DataPlayer dataPlayer, String section, String updatedColumn);
	public void onLocalDataPlayerUpdate(DataPlayer dataPlayer, String section, String updatedColumn);
	public void onPlayerUpdate(String playerName, String section, String updatedColumn);

}
