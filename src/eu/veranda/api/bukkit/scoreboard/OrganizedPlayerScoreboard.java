package eu.veranda.api.bukkit.scoreboard;

import java.util.HashMap;
import java.util.Map.Entry;

public class OrganizedPlayerScoreboard implements IScoreboard {
	
	private PlayerScoreboard scoreboard;
	public HashMap<Integer, String> values = new HashMap<Integer, String>();
	private int size;
	
	public OrganizedPlayerScoreboard(PlayerScoreboard scoreboard, int size) {
		if(size > 15) {
			throw new IllegalArgumentException("Size can not be larger than 15");
		}
		scoreboard.recreateSideObjective();
		
		this.scoreboard = scoreboard;
		this.size = size;

		reset();
		
		scoreboard.getSideObjective().setDisplayName(" ");
	}
	
	private String getEmptyString(int length) {
		String empty = "";
		for(int i = 0; i < length; i++) {
			empty += " ";
		}
		return empty;
	}
	
	public void reset() {
		for(String s : values.values()) {
			scoreboard.resetScores(s);
		}
		values.clear();
		for(int i = 1; i <= size; i++) {
			String empty = getEmptyString(i);
			scoreboard.getSideObjective().getScore(empty).setScore(i);
			values.put(i, empty);
		}
	}

	public void setSize(int size) {
		if(size > 15) {
			throw new IllegalArgumentException("Size can not be larger than 15");
		}
		
		values.clear();
		scoreboard.recreateSideObjective();
		
		for(int i = 1; i <= size; i++) {
			String empty = getEmptyString(i);
			scoreboard.getSideObjective().getScore(empty).setScore(i);
			values.put(i, empty);
		}
		
		scoreboard.getSideObjective().setDisplayName(" ");
	}

	public boolean set(int id, String value, boolean isTitle) {
		if(id > size) {
			return false;
		}
		
		if(isTitle) {
			value = value + getEmptyString(16 - value.length());
		}
		else {
			while(values.containsValue(value) && value.length() < 16) {
				value = value + " ";
			}
		}
		
		String scorename = values.get(id);
		if(scorename != null && !scorename.equalsIgnoreCase("null")) {
			this.scoreboard.resetScores(scorename);
		}
		scoreboard.getSideObjective().getScore(value).setScore(id);
		values.put(id, value);
		return true;
	}
	
	public boolean reset(int id) {
		if(id > size) {
			return false;
		}
		
		String scorename = values.get(id);
		if(scorename != null && !scorename.equalsIgnoreCase("null")) {
			scoreboard.resetScores(scorename);
		}
		scoreboard.getSideObjective().getScore(getEmptyString(id)).setScore(id);
		values.put(id, getEmptyString(id));
		return true;
	}
	
	public void setTitle(String title) {
		scoreboard.getSideObjective().setDisplayName(title);
	}
	
	public void update() {
		for(Entry<Integer, String> score : values.entrySet()) {
			scoreboard.getSideObjective().getScore(score.getValue()).setScore(score.getKey());
		}
	}

}
