package eu.veranda.api.bukkit.scoreboard;

import java.util.HashSet;
import java.util.UUID;

import org.bukkit.scoreboard.Team;

public class ScoreboardTeam {
	
	private final String name;
	private String prefix;
	private boolean friendlyFire;
	private boolean canSeeFriendlyInvisibles;
	private HashSet<String> players = new HashSet<String>();
	
	public ScoreboardTeam(String name) {
		this.name = name;
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	public String getName() {
		return name;
	}
	
	public void allowFriendlyFire(boolean flag) {
		this.friendlyFire = flag;
	}
	
	public void addPlayer(String name) {
		players.add(name);
	}
	
	public void removePlayer(String name) {
		players.remove(name);;
	}
	
	public void canSeeFriendlyInvisibles(boolean flag) {
		this.canSeeFriendlyInvisibles = flag;
	}
	
	public void setTeamOptions(Team team) {
		team.setPrefix(prefix);
		team.setAllowFriendlyFire(friendlyFire);
		team.setCanSeeFriendlyInvisibles(canSeeFriendlyInvisibles);
		for(String name : players) {
			team.addEntry(name);
		}
	}

	@Deprecated
	public void removePlayer(UUID uuid) {
		
	}

}
