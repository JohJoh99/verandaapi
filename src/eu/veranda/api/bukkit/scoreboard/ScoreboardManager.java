package eu.veranda.api.bukkit.scoreboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Team;

import eu.veranda.api.player.Rank;

public class ScoreboardManager {
	
	private static HashMap<UUID, PlayerScoreboard> scoreboards = new HashMap<UUID, PlayerScoreboard>();
	private static HashMap<UUID, IScoreboard> subScoreboards = new HashMap<UUID, IScoreboard>();
	private static HashMap<String, ScoreboardTeam> scoreboardTeams = new HashMap<String, ScoreboardTeam>();
	
	public static void addScoreboard(PlayerScoreboard scoreboard) {
		for(ScoreboardTeam t : scoreboardTeams.values()) {
			scoreboard.addTeam(t);
		}
		scoreboards.put(scoreboard.getPlayer().getUniqueId(), scoreboard);
	}
	
	public static PlayerScoreboard getScoreboard(Player player) {
		return scoreboards.get(player.getUniqueId());
	}
	
	public static PlayerScoreboard getScoreboard(UUID uniqueId) {
		return scoreboards.get(uniqueId);
	}
	
	public static void removeScoreboard(Player player) {
		scoreboards.remove(player.getUniqueId());
	}
	
	public static void removeScoreboard(UUID uniqueId) {
		scoreboards.remove(uniqueId);
	}
	
	public static void addSubScoreboard(UUID uniqueId, IScoreboard scoreboard) {
		subScoreboards.put(uniqueId, scoreboard);
	}
	
	public static IScoreboard getSubScoreboard(Player player) {
		return subScoreboards.get(player.getUniqueId());
	}
	
	public static IScoreboard getSubScoreboard(UUID uniqueId) {
		return subScoreboards.get(uniqueId);
	}
	
	public static void removeSubScoreboard(Player player) {
		subScoreboards.remove(player.getUniqueId());
	}
	
	public static void removeSubScoreboard(UUID uniqueId) {
		subScoreboards.remove(uniqueId);
	}
	
	public static List<PlayerScoreboard> getScoreboards() {
		ArrayList<PlayerScoreboard> scoreboards = new ArrayList<PlayerScoreboard>();
		scoreboards.addAll(ScoreboardManager.scoreboards.values());
		return scoreboards;
	}
	
	public static void addTeam(ScoreboardTeam team) {
		scoreboardTeams.put(team.getName(), team);
		List<PlayerScoreboard> scoreboards = getScoreboards();
		for(PlayerScoreboard s : scoreboards) {
			s.addTeam(team);
		}
	}
	
	public static void removeTeam(String name) {
		scoreboardTeams.remove(name);
		List<PlayerScoreboard> scoreboards = getScoreboards();
		for(PlayerScoreboard s : scoreboards) {
			s.removeTeam(name);
		}
	}
	
	public static void addPlayerToTeam(String teamName, Player p) {
		ScoreboardTeam scTeam = scoreboardTeams.get(teamName);
		scTeam.addPlayer(p.getName());
		List<PlayerScoreboard> scoreboards = getScoreboards();
		for(PlayerScoreboard s : scoreboards) {
			Team team = s.getTeam(teamName);
			if(team != null) {
				team.addEntry(p.getName());
			}
		}
	}
	
	public static void addPlayerToTeam(String teamName, String name) {
		ScoreboardTeam scTeam = scoreboardTeams.get(teamName);
		scTeam.addPlayer(name);
		List<PlayerScoreboard> scoreboards = getScoreboards();
		for(PlayerScoreboard s : scoreboards) {
			Team team = s.getTeam(teamName);
			if(team != null) {
				team.addEntry(name);
			}
		}
	}
	
	public static void removePlayerFromAllTeams(Player p) {
		for(String team : scoreboardTeams.keySet()) {
			removePlayerFromTeam(team, p);
		}
	}
	
	public static void removePlayerFromAllTeams(String name) {
		for(String team : scoreboardTeams.keySet()) {
			removePlayerFromTeam(team, name);
		}
	}
	
	public static void removePlayerFromTeam(String teamName, Player p) {
		ScoreboardTeam scTeam = scoreboardTeams.get(teamName);
		scTeam.removePlayer(p.getName());
		List<PlayerScoreboard> scoreboards = getScoreboards();
		for(PlayerScoreboard s : scoreboards) {
			Team team = s.getTeam(teamName);
			if(team != null) {
				team.removeEntry(p.getName());
			}
		}
	}
	
	public static void removePlayerFromTeam(String teamName, String name) {
		ScoreboardTeam scTeam = scoreboardTeams.get(teamName);
		scTeam.removePlayer(name);
		List<PlayerScoreboard> scoreboards = getScoreboards();
		for(PlayerScoreboard s : scoreboards) {
			Team team = s.getTeam(teamName);
			if(team != null) {
				team.removeEntry(name);
			}
		}
	}
	
	public static void addRankTeams() {
		for(Rank r : Rank.values()) {
			ScoreboardTeam scTeam = new ScoreboardTeam(r.getName());
			scTeam.setPrefix(r.getColor().toString());
			scTeam.allowFriendlyFire(true);
			addTeam(scTeam);
		}
	}
	
	public static void removeRankTeams() {
		for(Rank r : Rank.values()) {
			removeTeam(r.getName());
		}
	}
	
	public static void refreshTeams() {
		for(ScoreboardTeam team : scoreboardTeams.values()) {
			for(PlayerScoreboard scoreboard : getScoreboards()) {
				scoreboard.removeTeam(team.getName());
				scoreboard.addTeam(team);
			}
		}
	}

}
