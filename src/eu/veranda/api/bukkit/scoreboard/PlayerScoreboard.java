package eu.veranda.api.bukkit.scoreboard;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

public class PlayerScoreboard {
	
	private Player player;
	private Scoreboard scoreboard;
	private Objective sideObjective;
	private Objective tabObjective;
	private Objective belowNameObjective;
	
	public PlayerScoreboard(Player player) {
		this.player = player;
		this.scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
		this.sideObjective = scoreboard.registerNewObjective("side", "dummy");
		this.sideObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public Objective getSideObjective() {
		return sideObjective;
	}
	
	public Objective getTabObjective() {
		return tabObjective;
	}
	
	public Objective getBelowNameObjective() {
		return belowNameObjective;
	}
	
	public void recreateSideObjective() {
		if(this.sideObjective != null) {
			this.sideObjective.unregister();
			this.sideObjective = null;
		}
		
		this.sideObjective = scoreboard.registerNewObjective("side", "dummy");
		this.sideObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
	}
	
	public void recreateTabObjective() {
		if(this.tabObjective != null) {
			this.tabObjective.unregister();
			this.tabObjective = null;
		}
		
		this.tabObjective = scoreboard.registerNewObjective("tab", "dummy");
		this.tabObjective.setDisplaySlot(DisplaySlot.PLAYER_LIST);
	}
	
	public void recreateBelowNameObjective() {
		if(this.belowNameObjective != null) {
			this.belowNameObjective.unregister();
			this.belowNameObjective = null;
		}
		
		this.belowNameObjective = scoreboard.registerNewObjective("below_name", "dummy");
		this.belowNameObjective.setDisplaySlot(DisplaySlot.BELOW_NAME);
	}
	
	public void resetScores(String name) {
		this.scoreboard.resetScores(name);
	}

	public void show() {
		player.setScoreboard(scoreboard);
	}

	public void addTeam(ScoreboardTeam scTeam) {
		Team team = scoreboard.registerNewTeam(scTeam.getName());
		scTeam.setTeamOptions(team);
	}

	public void removeTeam(String name) {
		Team team = scoreboard.getTeam(name);
		if(team != null) team.unregister();
	}
	
	public Team getTeam(String name) {
		return scoreboard.getTeam(name);
	}

}
