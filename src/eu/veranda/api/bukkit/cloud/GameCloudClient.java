package eu.veranda.api.bukkit.cloud;

import java.sql.Connection;
import java.sql.SQLException;

import org.bukkit.Bukkit;

import eu.veranda.api.APIException;
import eu.veranda.api.VerandaAPI;
import eu.veranda.api.bukkit.VerandaAPIPlugin;
import eu.veranda.api.bukkit.player.PlayerStorage;
import eu.veranda.api.bukkit.player.VerandaPlayer;
import eu.veranda.api.player.DataPlayer;
import eu.veranda.api.player.DataPlayerStorage;
import eu.veranda.api.player.FriendUnit;
import eu.veranda.api.sql.MySQLConnectionUtils;
import eu.veranda.api.update.DPULManager;
import eu.veranda.cloud.client.ClientType;
import eu.veranda.cloud.client.CloudClient;
import eu.veranda.cloud.logging.Level;
import eu.veranda.cloud.logging.Logger;
import eu.veranda.cloud.client.ServerState;
import eu.veranda.cloud.packet.Packet;
import eu.veranda.cloud.packet.game.PacketClientGameExtraInformation;
import eu.veranda.cloud.packet.game.PacketClientGameMaxPlayers;
import eu.veranda.cloud.packet.game.PacketClientGamePlayerCount;
import eu.veranda.cloud.packet.game.PacketClientGameServerState;
import eu.veranda.cloud.packet.general.PacketRedirectGeneralPlayerUpdate;
import eu.veranda.cloud.packet.general.PacketServerGeneralStop;

public class GameCloudClient extends CloudClient {

	private String extraInformation;
	private ServerState onlineState = ServerState.LOBBY;
	
	public GameCloudClient(String password, String hostname, int port) {
		super(VerandaAPI.SERVER_NAME, ClientType.GAME_SERVER, password, hostname, port);
	}

	@Override
	public void onPacketReceive(Packet p) {
		if(p instanceof PacketRedirectGeneralPlayerUpdate) {
			PacketRedirectGeneralPlayerUpdate prgpu = (PacketRedirectGeneralPlayerUpdate) p;
			VerandaPlayer player = PlayerStorage.getRegistered(prgpu.getPlayer());
			DataPlayer dataPlayer = null;
			if(player != null) {
				dataPlayer = player.getDataPlayer();
			}
			else {
				dataPlayer = DataPlayerStorage.getRegisteredPlayer(prgpu.getPlayer());
			}
			
			if(dataPlayer != null) {
				if(prgpu.getSection().equals(DataPlayer.class.getName())) {
					Connection con = null;
					try {
						con = MySQLConnectionUtils.getNewConnection();
						dataPlayer.reloadData(con);
						DPULManager.callLocal(dataPlayer, prgpu.getSection(), prgpu.getUpdatedColumn());
					}
					catch(SQLException e) {
						Logger.log(Level.INFO, "Failed to update DataPlayer (" + e.getClass().getName() + ": " + e.getMessage() + ")", false);
					}
					finally {
						MySQLConnectionUtils.closeConnection(con);
					}
				}
				else if(prgpu.getSection().equals(FriendUnit.class.getName()) && dataPlayer.getFriendUnit() != null) {
					Connection con = null;
					try {
						con = MySQLConnectionUtils.getNewConnection();
						dataPlayer.getFriendUnit().reloadData(con);
						DPULManager.callLocal(dataPlayer, prgpu.getSection(), prgpu.getUpdatedColumn());
					}
					catch(SQLException | APIException e) {
						Logger.log(Level.INFO, "Failed to update FriendUnit (" + e.getClass().getName() + ": " + e.getMessage() + ")", false);
					}
					finally {
						MySQLConnectionUtils.closeConnection(con);
					}
				}
			}
		}
		else if(p instanceof PacketServerGeneralStop) {
			VerandaAPIPlugin.shutdown();
		}
	}
	
	@Override
	public void onConnect() {
		new Thread() {
			
			@Override
			public void run() {
				Thread.currentThread().setName("ConnectSender");
				try {
					Thread.sleep(3000);
				}
				catch (InterruptedException e) {}
				
				Logger.log(Level.INFO, "Sending lobby information packets...", false);
				sendPacket(new PacketClientGameMaxPlayers(Bukkit.getMaxPlayers()));
				sendPacket(new PacketClientGamePlayerCount(PlayerStorage.getPlayers().size(), PlayerStorage.getSpectators().size()));
				sendPacket(new PacketClientGameServerState(onlineState));
				if(extraInformation != null) {
					sendPacket(new PacketClientGameExtraInformation(extraInformation));
				}
			}
			
		}.start();
	}
	
	public void setExtraInformation(String extraInformation) {
		this.extraInformation = extraInformation;
		sendPacket(new PacketClientGameExtraInformation(extraInformation));
	}
	
	public void setOnlineState(ServerState onlineState) {
		this.onlineState = onlineState;
		sendPacket(new PacketClientGameServerState(onlineState));
	}

}
