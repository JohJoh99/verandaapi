package eu.veranda.api.bukkit.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import eu.veranda.api.bukkit.player.VerandaPlayer;

public class PlayerRegisteredEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final VerandaPlayer verandaPlayer;
	
	public PlayerRegisteredEvent(VerandaPlayer verandaPlayer) {
		this.verandaPlayer = verandaPlayer;
	}
	
	public VerandaPlayer getVerandaPlayer() {
		return verandaPlayer;
	}
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
