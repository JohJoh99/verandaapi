package eu.veranda.api.bukkit.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import eu.veranda.api.bukkit.player.VerandaPlayer;

public class PlayerRegisterEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final VerandaPlayer verandaPlayer;
	private boolean registerPlayer = true;
	
	public PlayerRegisterEvent(VerandaPlayer verandaPlayer) {
		this.verandaPlayer = verandaPlayer;
	}
	
	public VerandaPlayer getVerandaPlayer() {
		return verandaPlayer;
	}
	
	public void registerAsSpectator() {
		registerPlayer = false;
	}
	
	public void registerAsPlayer() {
		registerPlayer = true;
	}
	
	public boolean getRegisterAsPlayer() {
		return registerPlayer;
	}
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
