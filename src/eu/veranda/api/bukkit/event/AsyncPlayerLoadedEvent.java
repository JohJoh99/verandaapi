package eu.veranda.api.bukkit.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

import eu.veranda.api.player.DataPlayer;

public class AsyncPlayerLoadedEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final DataPlayer dataPlayer;
	private AsyncPlayerPreLoginEvent.Result loginResult = AsyncPlayerPreLoginEvent.Result.ALLOWED;
	private String kickMessage = null;
	
	public AsyncPlayerLoadedEvent(DataPlayer dataPlayer) {
		this.dataPlayer = dataPlayer;
	}
	
	public void allow() {
		loginResult = AsyncPlayerPreLoginEvent.Result.ALLOWED;
	}
	
	public void disallow(AsyncPlayerPreLoginEvent.Result loginResult, String kickMessage) {
		this.loginResult = loginResult;
		this.kickMessage = kickMessage;
	}
	
	public void setLoginResult(AsyncPlayerPreLoginEvent.Result loginResult) {
		this.loginResult = loginResult;
	}
	
	public void setKickMessage(String kickMessage) {
		this.kickMessage = kickMessage;
	}
	
	public AsyncPlayerPreLoginEvent.Result getLoginResult() {
		return loginResult;
	}
	
	public String getKickMessage() {
		return kickMessage;
	}
	
	public DataPlayer getDataPlayer() {
		return dataPlayer;
	}
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
