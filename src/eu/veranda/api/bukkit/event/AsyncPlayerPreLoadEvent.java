package eu.veranda.api.bukkit.event;

import java.util.UUID;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;

public class AsyncPlayerPreLoadEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final UUID playerUuid;
//	private boolean loadFriends = false;
//	private boolean loadStats = false;
	private AsyncPlayerPreLoginEvent.Result loginResult = AsyncPlayerPreLoginEvent.Result.ALLOWED;
	private String kickMessage = null;
//	private GameType[] gameTypes;
	
	public AsyncPlayerPreLoadEvent(UUID playerUuid) {
		this.playerUuid = playerUuid;
	}
	
//	public void setLoadFriends(boolean flag) {
//		this.loadFriends = flag;
//	}
//	
//	public void setLoadStats(boolean flag, GameType... gameTypes) {
//		this.loadStats = flag;
//		this.gameTypes = gameTypes;
//	}
	
	public void allow() {
		loginResult = AsyncPlayerPreLoginEvent.Result.ALLOWED;
	}
	
	public void disallow(AsyncPlayerPreLoginEvent.Result loginResult, String kickMessage) {
		this.loginResult = loginResult;
		this.kickMessage = kickMessage;
	}
	
	public void setLoginResult(AsyncPlayerPreLoginEvent.Result loginResult) {
		this.loginResult = loginResult;
	}
	
	public void setKickMessage(String kickMessage) {
		this.kickMessage = kickMessage;
	}
	
	public AsyncPlayerPreLoginEvent.Result getLoginResult() {
		return loginResult;
	}
	
	public String getKickMessage() {
		return kickMessage;
	}
	
	public UUID getUniqueId() {
		return playerUuid;
	}
	
//	public boolean getLoadFriends() {
//		return loadFriends;
//	}
//	
//	public boolean getLoadStats() {
//		return loadStats;
//	}
//	
//	public GameType[] getGameTypes() {
//		return gameTypes;
//	}
	
	public HandlerList getHandlers() {
	    return handlers;
	}
	 
	public static HandlerList getHandlerList() {
	    return handlers;
	}

}
