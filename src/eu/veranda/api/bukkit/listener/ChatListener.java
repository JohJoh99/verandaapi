package eu.veranda.api.bukkit.listener;

import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.Plugin;

import eu.veranda.api.APIUtil;
import eu.veranda.api.VerandaAPI;
import eu.veranda.api.bukkit.player.PlayerStorage;
import eu.veranda.api.bukkit.player.VerandaPlayer;
import eu.veranda.api.player.Rank;

public class ChatListener implements Listener {
	
	private static HashMap<String, Rank> whitelistedCommands = new HashMap<String, Rank>();
	
	public ChatListener(Plugin plugin) {
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler
	public void onAsyncPlayerChat(AsyncPlayerChatEvent e) {
		// TODO
//		VerandaAPI.log(Level.INFO, "[Chat]", e.getPlayer().getUniqueId() + "/" + e.getPlayer().getName() + ": " + e.getMessage());
		e.setCancelled(true);
		Bukkit.broadcastMessage(PlayerStorage.getRegistered(e.getPlayer()).getRank().getColor() + e.getPlayer().getDisplayName() + ChatColor.DARK_GRAY + " \u00BB " + ChatColor.WHITE + e.getMessage());
	}
	
	@EventHandler
	public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
		if(!e.isCancelled()) {
			if(VerandaAPI.SERVER_NAME.toUpperCase().contains("BUILD")) {
				return;
			}
			Player player = e.getPlayer();
			String cmd = e.getMessage().split(" ")[0].replace("/", "");
			VerandaPlayer p = PlayerStorage.getRegistered(player);
			if(cmd.toLowerCase().equals("stop")) {
				if(p.hasRank(Rank.DEVELOPER)) {
					for(Player oPlayer : Bukkit.getOnlinePlayers()) {
						oPlayer.kickPlayer(ChatColor.RED + "Server is restarting!");
					}
					
					Bukkit.shutdown();
					
					e.setCancelled(true);
				}
				else {
					player.sendMessage(VerandaAPI.VERANDA_PREFIX + ChatColor.RED + "Dieser Befehl existiert nicht!");
					e.setCancelled(true);
				}
			}
			else if(!whitelistedCommands.containsKey(cmd.toLowerCase())) {
				player.sendMessage(VerandaAPI.VERANDA_PREFIX + ChatColor.RED + "Dieser Befehl existiert nicht!");
				e.setCancelled(true);
			}
		}
	}
	
	public static void whitelistCommand(String name, Rank rank) {
		whitelistedCommands.put(name, rank);
	}
	
	public static void unwhitelistCommand(String command) {
		whitelistedCommands.remove(command.toLowerCase());
	}
	
	public static List<String> getWhitelistedCommands() {
		return APIUtil.copy(whitelistedCommands.keySet());
	}
	
	public static Rank getCommandRank(String command) {
		return whitelistedCommands.get(command);
	}

}
