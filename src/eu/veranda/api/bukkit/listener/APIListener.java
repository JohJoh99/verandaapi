package eu.veranda.api.bukkit.listener;

import java.sql.SQLException;
import java.util.HashSet;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import eu.veranda.api.APIException;
import eu.veranda.api.VerandaAPI;
import eu.veranda.api.bukkit.VerandaAPIPlugin;
import eu.veranda.api.bukkit.event.AsyncPlayerLoadedEvent;
import eu.veranda.api.bukkit.event.AsyncPlayerPreLoadEvent;
import eu.veranda.api.bukkit.event.PlayerRegisterEvent;
import eu.veranda.api.bukkit.event.PlayerRegisteredEvent;
import eu.veranda.api.bukkit.player.PlayerStorage;
import eu.veranda.api.bukkit.player.VerandaPlayer;
import eu.veranda.api.bukkit.scoreboard.PlayerScoreboard;
import eu.veranda.api.bukkit.scoreboard.ScoreboardManager;
import eu.veranda.api.player.DataPlayer;
import eu.veranda.api.player.DataPlayerStorage;
import eu.veranda.cloud.packet.game.PacketClientGamePlayerCount;

public class APIListener implements Listener {
	
	private long enableStart;
	private HashSet<UUID> joinDisallowed = new HashSet<UUID>();
	
	public APIListener(Plugin plugin) {
		enableStart = System.currentTimeMillis();
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onAsyncPlayerPreLogin(AsyncPlayerPreLoginEvent e) {
		if(e.getLoginResult() != Result.ALLOWED) return;
		if(enableStart + 3000 > System.currentTimeMillis()) {
			e.setKickMessage(ChatColor.RED + "Server is initializing!");
			e.setLoginResult(Result.KICK_OTHER);
			return;
		}
		if(joinDisallowed.contains(e.getUniqueId())) {
			e.disallow(Result.KICK_OTHER, ChatColor.RED + "Bitte warte etwas bevor du dich erneut verbindest!");
			return;
		}
		AsyncPlayerPreLoadEvent event = new AsyncPlayerPreLoadEvent(e.getUniqueId());
		event.setLoginResult(e.getLoginResult());
		event.setKickMessage(e.getKickMessage());
		Bukkit.getPluginManager().callEvent(event);
		e.setLoginResult(event.getLoginResult());
		e.setKickMessage(event.getKickMessage());
		if(event.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED) {
			return;
		}
		
		DataPlayer dataPlayer = null;
		try {
			dataPlayer = new DataPlayer(e.getUniqueId(), e.getName(), true);
		}
		catch(SQLException | APIException exc) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, exc);
			e.disallow(Result.KICK_OTHER, ChatColor.RED + "We're sorry but an unexpected error occurred while connecting to this Server");
			return;
		}
		DataPlayerStorage.registerPlayer(dataPlayer);
		AsyncPlayerLoadedEvent event2 = new AsyncPlayerLoadedEvent(dataPlayer);
		event2.setLoginResult(e.getLoginResult());
		event2.setKickMessage(e.getKickMessage());
		Bukkit.getPluginManager().callEvent(event2);
		e.setLoginResult(event2.getLoginResult());
		e.setKickMessage(event2.getKickMessage());
		if(event2.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED) {
			DataPlayerStorage.unregisterPlayer(dataPlayer.getUniqueId());
			return;
		}
		else {
			if(dataPlayer.getAutoNickEnabled()) {
				
			}
		}
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerLogin(PlayerLoginEvent e) {
		if(DataPlayerStorage.getRegisteredPlayer(e.getPlayer().getUniqueId()) == null) {
			e.disallow(PlayerLoginEvent.Result.KICK_OTHER, ChatColor.RED + "We're sorry but an unexpected error occurred while connecting to this Server");
			return;
		}
		VerandaPlayer player = new VerandaPlayer(e.getPlayer(), DataPlayerStorage.getRegisteredPlayer(e.getPlayer().getUniqueId()));
		DataPlayerStorage.unregisterPlayer(e.getPlayer().getUniqueId());
		PlayerRegisterEvent pre = new PlayerRegisterEvent(player);
		
		Bukkit.getPluginManager().callEvent(pre);
		
		if(pre.getRegisterAsPlayer()) {
			PlayerStorage.registerPlayer(player);
		}
		else {
			PlayerStorage.registerSpectator(player);
		}
		
		//player.setDisplayName(player.getRank().getColor() + player.getName()); TODO
		player.setDisplayName(player.getName());
		
		if(VerandaAPIPlugin.getCloudClient() != null) {
			VerandaAPIPlugin.getCloudClient().sendPacket(new PacketClientGamePlayerCount(PlayerStorage.getPlayers().size(), PlayerStorage.getSpectators().size()));
		}
		
		Bukkit.getPluginManager().callEvent(new PlayerRegisteredEvent(player));
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerJoin(PlayerJoinEvent e) {
		if(!PlayerStorage.isRegistered(e.getPlayer())) {
			e.getPlayer().kickPlayer(ChatColor.RED + "We're sorry but an unexpected error occurred while connecting to this Server");
			return;
		}
		
		VerandaPlayer player = PlayerStorage.getRegistered(e.getPlayer());
		
		PlayerScoreboard scoreboard = new PlayerScoreboard(player);
		ScoreboardManager.addScoreboard(scoreboard);
		ScoreboardManager.addPlayerToTeam(player.getRank().getName(), e.getPlayer());
		scoreboard.show();
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerQuitLow(PlayerQuitEvent e) {
		UUID uuid = e.getPlayer().getUniqueId();
		joinDisallowed.add(uuid);
		Bukkit.getScheduler().scheduleSyncDelayedTask(VerandaAPIPlugin.getInstance(), new Runnable() {
			
			public void run() {
				joinDisallowed.remove(uuid);
			}
			
		}, 3 * 20L);
	}
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = false)
	public void onPlayerQuit(PlayerQuitEvent e) {
		
		PlayerStorage.unregister(e.getPlayer());
		ScoreboardManager.removeScoreboard(e.getPlayer());
		ScoreboardManager.removePlayerFromAllTeams(e.getPlayer());
		
		if(VerandaAPIPlugin.getCloudClient() != null) {
			VerandaAPIPlugin.getCloudClient().sendPacket(new PacketClientGamePlayerCount(PlayerStorage.getPlayers().size(), PlayerStorage.getSpectators().size()));
		}
	}

}
