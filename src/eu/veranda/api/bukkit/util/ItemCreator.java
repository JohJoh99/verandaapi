package eu.veranda.api.bukkit.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemCreator {
	
	public static ItemStack namedItem(Material m, String name) {
		return namedItem(m, name, new ArrayList<String>(), 1, 0);
	}
	
	public static ItemStack namedItem(Material m, String name, int amount, int durability) {
		return namedItem(m, name, new ArrayList<String>(), amount, durability);
	}
	
	public static ItemStack namedItem(Material m, String name, String[] lore) {
		return namedItem(m, name, Arrays.asList(lore), 1, (short) 0);
	}
	
	public static ItemStack namedItem(Material m, String name, List<String> lore) {
		return namedItem(m, name, lore, 1, 0);
	}
	
	public static ItemStack namedItem(Material m, String name, List<String> lore, int amount, int durability) {
		ItemStack is = new ItemStack(m, amount);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(name);
		if(lore != null && !lore.isEmpty()) {
			meta.setLore(lore);
		}
		else {
			meta.setLore(null);
		}
		is.setItemMeta(meta);
		is.setDurability((short) durability);
		return is;
	}

}
