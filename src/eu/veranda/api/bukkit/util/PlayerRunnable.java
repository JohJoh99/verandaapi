package eu.veranda.api.bukkit.util;

import eu.veranda.api.bukkit.player.VerandaPlayer;

public interface PlayerRunnable {
	
	public void run(VerandaPlayer player);

}
