package eu.veranda.api.bukkit.util;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import eu.veranda.api.VerandaAPI;

public class WorldLoader {
	
	public static boolean deleteWorld(String worldName) {
		if(Bukkit.getWorld(worldName) != null) {
			Bukkit.unloadWorld(worldName, false);
		}
		
		if(Bukkit.getWorld(worldName) != null) {
			VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "World could not be unloaded!");
			return false;
		}
		
		File worldFolder = new File(Bukkit.getWorldContainer().getAbsoluteFile().getParentFile() + File.separator + worldName);
		if(worldFolder.exists()) {
			if(worldFolder.isDirectory()) {
				try {
					FileUtils.deleteDirectory(worldFolder);
				}
				catch (IOException e) {
					return false;
				}
			}
			else {
				return worldFolder.delete();
			}
		}
		return true;
	}
	
	public static boolean loadWorldToServerDirectory(File copyFrom, String worldName) {
		File worldFolder = new File(Bukkit.getWorldContainer().getAbsoluteFile().getParentFile() + File.separator + worldName);
		
		if(!copyFrom.exists()) {
			VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "Map Folder does not Exist!");
			return false;
		}
		
		if(Bukkit.getWorld(worldName) != null) {
			VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "World is already loaded! To prevent double loading on startup, the World can't be unloaded now.");
			return false;
		}
		
		try {
			FileUtils.deleteDirectory(worldFolder);
		}
		catch (IOException e) {
			VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "Failed to delete World! (" + e.getMessage() + ")");
			return false;
		}
		
		try {
			FileUtils.copyDirectory(copyFrom, worldFolder);
		} 
		catch (IOException e) {
			VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "Failed to copy World! (" + e.getMessage() + ")");
			return false;
		}
		
		return true;
	}
	
	public static boolean loadWorldToFileDirectory(World world, File copyTo, String worldName) {
		File worldFolder = world.getWorldFolder();
		
		if(!worldFolder.exists()) {
			VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "Map Folder does not Exist!");
			return false;
		}
		
		try {
			FileUtils.copyDirectory(worldFolder, copyTo);
		} 
		catch (IOException e) {
			VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "Failed to copy World! (" + e.getMessage() + ")");
			return false;
		}
		
		return true;
	}
	
	public static void loadWorldIntoBukkit(String worldName) {
		WorldCreator wc = new WorldCreator(worldName);
		Bukkit.getServer().createWorld(wc);
	}

}
