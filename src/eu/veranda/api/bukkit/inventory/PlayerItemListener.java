package eu.veranda.api.bukkit.inventory;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

import eu.veranda.api.bukkit.inventory.InteractableItem.MetaObject;
import eu.veranda.api.bukkit.player.PlayerStorage;

public class PlayerItemListener implements Listener {
	
	private static HashSet<String> disabledInventories = new HashSet<String>();
	
	private static PlayerItemListener instance;
	
	public static PlayerItemListener getInstance() {
		return instance;
	}
	
	public static void disableInventory(String name) {
		disabledInventories.add(name);
	}
	
	public static void enableInventory(String name) {
		disabledInventories.remove(name);
	}
	
	private HashMap<MetaObject, InteractableItem> items = new HashMap<MetaObject, InteractableItem>();
	
	public PlayerItemListener(Plugin plugin) {
		if(instance != null) throw new IllegalStateException("PlayerItemListener has already been initialized");
		instance = this;
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}
	
	protected void addItem(InteractableItem item) {
		items.put(item.getMetaObject(), item);
	}
	
	protected void removeItem(MetaObject object) {
		items.remove(object);
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if(e.getItem() != null && e.getItem().getType() != Material.AIR && e.getItem().hasItemMeta()) {
			MetaObject mo = new MetaObject(e.getItem().getType(), e.getItem().getItemMeta(), e.getItem().getAmount(), e.getItem().getDurability());
			InteractableItem item = items.get(mo);
			if(item != null) {
				if(item.getRunOnInteract() && item.getRunOnInteractActions().contains(e.getAction())) {
					e.setCancelled(true);
					if(PlayerStorage.isPlayerRegistered(e.getPlayer())) {
						item.run(PlayerStorage.getRegistered(e.getPlayer()));
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		if(e.getCurrentItem() == null && e.getWhoClicked() instanceof Player) return;
		
		if(e.getCurrentItem().hasItemMeta() && e.getCurrentItem().getItemMeta().getDisplayName() != null && e.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.BLACK.toString())) {
			e.setCancelled(true);
		}
		
		if(e.getCurrentItem().getType() != Material.AIR && e.getCurrentItem().hasItemMeta()) {
			MetaObject mo = new MetaObject(e.getCurrentItem().getType(), e.getCurrentItem().getItemMeta(), e.getCurrentItem().getAmount(), e.getCurrentItem().getDurability());
			InteractableItem item = items.get(mo);
			if(item != null) {
				if(item.getRunOnClick()) {
					e.setCancelled(true);
					if(PlayerStorage.isPlayerRegistered((Player) e.getWhoClicked())) {
						item.run(PlayerStorage.getRegistered((Player) e.getWhoClicked()));
					}
				}
			}
		}
	}

}
