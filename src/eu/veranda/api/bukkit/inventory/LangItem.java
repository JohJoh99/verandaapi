package eu.veranda.api.bukkit.inventory;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import eu.veranda.api.bukkit.player.VerandaPlayer;
import eu.veranda.api.lang.LanguageUtils;

public class LangItem extends ItemStack {
	
	private int language;
	private String keyName;
	private String[] keyLore;
	private ItemMeta meta;
	
	public LangItem(VerandaPlayer p, Material m, String keyName, int amount, short damage, String... keyLore) {
		super(m, amount, damage);
		this.language = p.getLanguage();
		this.keyName = keyName;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(VerandaPlayer p, Material m, String keyName, String... keyLore) {
		super(m);
		this.language = p.getLanguage();
		this.keyName = keyName;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(VerandaPlayer p, Material m, String keyName, int amount, String... keyLore) {
		super(m, amount);
		this.language = p.getLanguage();
		this.keyName = keyName;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(int language, Material m, String keyName, int amount, short damage, String... keyLore) {
		super(m, amount, damage);
		this.language = language;
		this.keyName = keyName;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(int language, Material m, String keyName, String... keyLore) {
		super(m);
		this.language = language;
		this.keyName = keyName;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(int language, Material m, String keyName, int amount, String... keyLore) {
		super(m, amount);
		this.language = language;
		this.keyName = keyName;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(VerandaPlayer p, Material m, String keyName, int amount, short damage, ItemMeta meta, String... keyLore) {
		super(m, amount, damage);
		this.language = p.getLanguage();
		this.keyName = keyName;
		this.meta = meta;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(VerandaPlayer p, Material m, String keyName, ItemMeta meta, String... keyLore) {
		super(m);
		this.language = p.getLanguage();
		this.keyName = keyName;
		this.meta = meta;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(VerandaPlayer p, Material m, String keyName, int amount, ItemMeta meta, String... keyLore) {
		super(m, amount);
		this.language = p.getLanguage();
		this.keyName = keyName;
		this.meta = meta;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(int language, Material m, String keyName, int amount, short damage, ItemMeta meta, String... keyLore) {
		super(m, amount, damage);
		this.language = language;
		this.keyName = keyName;
		this.meta = meta;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(int language, Material m, String keyName, ItemMeta meta, String... keyLore) {
		super(m);
		this.language = language;
		this.keyName = keyName;
		this.meta = meta;
		this.keyLore = keyLore;
		constructor();
	}
	
	public LangItem(int language, Material m, String keyName, int amount, ItemMeta meta, String... keyLore) {
		super(m, amount);
		this.language = language;
		this.keyName = keyName;
		this.meta = meta;
		this.keyLore = keyLore;
		constructor();
	}
	
	private void constructor() {
		ItemMeta meta = this.meta != null ? this.meta : super.getItemMeta();
		if(keyLore != null && keyLore.length > 0) {
			ArrayList<String> lore = new ArrayList<String>();
			for(String s : keyLore) {
				if(s.equals("")) {
					lore.add("");
					continue;
				}
				
				String loreString = LanguageUtils.getTranslation(s, language);
				lore.add(loreString);
			}
			meta.setLore(lore);	
		}
		
		if(keyName != null) {
			String name = LanguageUtils.getTranslation(keyName, language);
			meta.setDisplayName(name);
		}
		
		super.setItemMeta(meta);
	}
	
	public int getLanguage() {
		return language;
	}

}
