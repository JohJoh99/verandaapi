package eu.veranda.api.bukkit.inventory;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.event.block.Action;
import org.bukkit.inventory.meta.ItemMeta;

import eu.veranda.api.bukkit.player.VerandaPlayer;
import eu.veranda.api.bukkit.util.PlayerRunnable;

public class InteractableItem extends LangItem {

	private final PlayerRunnable runnable;
	private boolean runOnInteract = true;
	private boolean runOnClick = true;
	private ArrayList<Action> interactActions = new ArrayList<Action>();
	
	public InteractableItem(VerandaPlayer player, Material m, String keyName, int amount, short damage, PlayerRunnable runnable, String... keyLore) {
		super(player, m, keyName, amount, damage, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(VerandaPlayer player, Material m, String keyName, int amount, PlayerRunnable runnable, String... keyLore) {
		super(player, m, keyName, amount, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(VerandaPlayer player, Material m, String keyName, PlayerRunnable runnable, String... keyLore) {
		super(player, m, keyName, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(int language, Material m, String keyName, int amount, short damage, PlayerRunnable runnable, String... keyLore) {
		super(language, m, keyName, amount, damage, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(int language, Material m, String keyName, int amount, PlayerRunnable runnable, String... keyLore) {
		super(language, m, keyName, amount, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(int language, Material m, String keyName, PlayerRunnable runnable, String... keyLore) {
		super(language, m, keyName, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(VerandaPlayer player, Material m, String keyName, int amount, short damage, ItemMeta meta, PlayerRunnable runnable, String... keyLore) {
		super(player, m, keyName, amount, damage, meta, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(VerandaPlayer player, Material m, String keyName, int amount, ItemMeta meta, PlayerRunnable runnable, String... keyLore) {
		super(player, m, keyName, amount, meta, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(VerandaPlayer player, Material m, String keyName, ItemMeta meta, PlayerRunnable runnable, String... keyLore) {
		super(player, m, keyName, meta, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(int language, Material m, String keyName, int amount, short damage, ItemMeta meta, PlayerRunnable runnable, String... keyLore) {
		super(language, m, keyName, amount, damage, meta, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(int language, Material m, String keyName, int amount, ItemMeta meta, PlayerRunnable runnable, String... keyLore) {
		super(language, m, keyName, amount, meta, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	public InteractableItem(int language, Material m, String keyName, PlayerRunnable runnable, ItemMeta meta, String... keyLore) {
		super(language, m, keyName, meta, keyLore);
		this.runnable = runnable;
		interactActions.add(Action.RIGHT_CLICK_AIR);
		interactActions.add(Action.RIGHT_CLICK_BLOCK);
		register();
	}
	
	private void register() {
		PlayerItemListener.getInstance().addItem(this);
	}
	
	public void unregister() {
		PlayerItemListener.getInstance().removeItem(this.getMetaObject());
	}
	
	@Override
	public boolean setItemMeta(ItemMeta itemMeta) {
		unregister();
		boolean b = super.setItemMeta(itemMeta);
		register();
		return b;
	}
	
	public MetaObject getMetaObject() {
		return new MetaObject(this.getType(), this.getItemMeta(), this.getAmount(), this.getDurability());
	}
	
	public void runOnInteract(boolean flag, Action... interactActions) {
		this.runOnInteract = flag;
		this.interactActions.clear();
		if(this.runOnInteract && interactActions != null) {
			for(Action a : interactActions) this.interactActions.add(a);
		}
	}
	
	public void runOnClick(boolean flag) {
		this.runOnClick = flag;
	}
	
	public boolean getUnregisterAfterInvClose() {
		return true;
	}
	
	public boolean getRunOnInteract() {
		return this.runOnInteract;
	}
	
	public ArrayList<Action> getRunOnInteractActions() {
		return this.interactActions;
	}
	
	public boolean getRunOnClick() {
		return this.runOnClick;
	}
	
	protected void run(VerandaPlayer player) {
		runnable.run(player);
	}
	
	public static class MetaObject {
		
		private final Material itemMaterial;
		private final ItemMeta meta;
		private final int amount;
		private final short damage;
		
		public MetaObject(Material itemMaterial, ItemMeta meta, int amount, short damage) {
			this.itemMaterial = itemMaterial;
			this.meta = meta;
			this.amount = amount;
			this.damage = damage;
		}
		
		public Material getMaterial() {
			
			return itemMaterial;
		}
		
		public ItemMeta getMeta() {
			return meta;
		}
		
		public int getAmount() {
			return amount;
		}
		
		public short getDamage() {
			return damage;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + amount;
			result = prime * result + damage;
			result = prime * result + ((itemMaterial == null) ? 0 : itemMaterial.hashCode());
			result = prime * result + ((meta == null) ? 0 : meta.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			MetaObject other = (MetaObject) obj;
			if (amount != other.amount)
				return false;
			if (damage != other.damage)
				return false;
			if (itemMaterial != other.itemMaterial)
				return false;
			if (meta == null) {
				if (other.meta != null)
					return false;
			} else if (!meta.equals(other.meta))
				return false;
			return true;
		}

	}

}
