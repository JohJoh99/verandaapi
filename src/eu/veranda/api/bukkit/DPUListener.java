package eu.veranda.api.bukkit;

import eu.veranda.api.VerandaAPI;
import eu.veranda.api.bukkit.player.PlayerStorage;
import eu.veranda.api.bukkit.player.VerandaPlayer;
import eu.veranda.api.bukkit.scoreboard.ScoreboardManager;
import eu.veranda.api.player.DataPlayer;
import eu.veranda.api.update.DPULManager;
import eu.veranda.api.update.DataPlayerUpdateListener;
import eu.veranda.cloud.packet.general.PacketRedirectGeneralNamePlayerUpdate;
import eu.veranda.cloud.packet.general.PacketRedirectGeneralPlayerUpdate;

public class DPUListener implements DataPlayerUpdateListener {

	public DPUListener() {
		DPULManager.addListener(this);
	}
	
	@Override
	public void onDataPlayerUpdate(DataPlayer dataPlayer, String section, String updatedColumn) {
		VerandaAPIPlugin.getCloudClient().sendPacket(new PacketRedirectGeneralPlayerUpdate(dataPlayer.getUniqueId(), section, updatedColumn, VerandaAPI.SERVER_NAME));
		onLocalDataPlayerUpdate(dataPlayer, section, updatedColumn);
	}

	@Override
	public void onLocalDataPlayerUpdate(DataPlayer dataPlayer, String section, String updatedColumn) {
		if(PlayerStorage.isPlayerRegistered(dataPlayer.getUniqueId())) {
			VerandaPlayer player = PlayerStorage.getRegistered(dataPlayer.getUniqueId());
			if(updatedColumn.equals(VerandaAPI.COLUMN_RANK)) {
				ScoreboardManager.removePlayerFromAllTeams(player);
				ScoreboardManager.addPlayerToTeam(player.getRank().getName(), player);
			}
		}
		
	}

	@Override
	public void onPlayerUpdate(String playerName, String section, String updatedColumn) {
		VerandaAPIPlugin.getCloudClient().sendPacket(new PacketRedirectGeneralNamePlayerUpdate(playerName, section, updatedColumn, VerandaAPI.SERVER_NAME));
	}

}
