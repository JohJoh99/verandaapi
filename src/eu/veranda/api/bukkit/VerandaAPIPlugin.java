package eu.veranda.api.bukkit;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import eu.veranda.api.VerandaAPI;
import eu.veranda.api.bukkit.cloud.GameCloudClient;
import eu.veranda.api.bukkit.cmd.PluginsCommand;
import eu.veranda.api.bukkit.cmd.RAMCommand;
import eu.veranda.api.bukkit.inventory.PlayerItemListener;
import eu.veranda.api.bukkit.listener.APIListener;
import eu.veranda.api.bukkit.listener.ChatListener;
import eu.veranda.api.bukkit.scoreboard.ScoreboardManager;
import eu.veranda.api.lang.LanguageUtils;
import eu.veranda.api.player.DataPlayer;
import eu.veranda.api.player.Rank;
import eu.veranda.api.sql.MySQLConnectionUtils;
import eu.veranda.api.update.DPULManager;
import eu.veranda.api.update.DataPlayerUpdateListener;
import eu.veranda.cloud.packet.general.PacketRedirectGeneralNamePlayerUpdate;
import eu.veranda.cloud.packet.general.PacketRedirectGeneralPlayerUpdate;

public class VerandaAPIPlugin extends JavaPlugin {
	
	private static boolean loadError = false;
	private static boolean initializeCloudClient = true;
	private static GameCloudClient cloudClient;
	private static VerandaAPIPlugin instance;
	
	public VerandaAPIPlugin() {
		instance = this;
	}
	
	@Override
	public void onLoad() {
		VerandaAPI.init();
		MySQLConnectionUtils.reloadCfgFromProperties();
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			
			LanguageUtils.initialize(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		MySQLConnectionUtils.closeConnection(con);
	}
	
	@Override
	public void onEnable() {
		
		if(hasLoadErrors()) {
			return;
		}
		
		if(initializeCloudClient) {
			VerandaAPI.log(Level.INFO, VerandaAPI.API_PREFIX, "Starting CloudClient...");
			cloudClient = new GameCloudClient("password_safe", "localhost", 4242); // TODO
			cloudClient.startAsThread();
			
			DPULManager.addListener(new DataPlayerUpdateListener() {
				
				@Override
				public void onDataPlayerUpdate(DataPlayer dataPlayer, String section, String updatedColumn) {
					cloudClient.sendPacket(new PacketRedirectGeneralPlayerUpdate(dataPlayer.getUniqueId(), section, updatedColumn, VerandaAPI.SERVER_NAME));
				}

				@Override
				public void onLocalDataPlayerUpdate(DataPlayer dataPlayer, String section, String updatedColumn) {}

				@Override
				public void onPlayerUpdate(String playerName, String section, String updatedColumn) {
					cloudClient.sendPacket(new PacketRedirectGeneralNamePlayerUpdate(playerName, section, updatedColumn, VerandaAPI.SERVER_NAME));
				}
				
			});
			
			new DPUListener();
		}
		
		ScoreboardManager.addRankTeams();
		
		new APIListener(this);
		new ChatListener(this);
		new PlayerItemListener(this);
		new PlMessageListener(this);
		
		new RAMCommand(this);
		new PluginsCommand(this);
		
		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "Veranda");
		
		System.out.println("[VerandaAPI] Plugin enabled!");
	}
	
	@Override
	public void onDisable() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			p.kickPlayer(ChatColor.RED + "Server is restarting!");
		}
		
		if(cloudClient != null) {
			cloudClient.setAutoReconnect(false);
			cloudClient.close(true, "Minecraft Server is shutting down!");
		}
		
		System.out.println("[VerandaAPI] Plugin disabled!");
	}
	
	public static VerandaAPIPlugin getInstance() {
		return instance;
	}
	
	public static boolean hasLoadErrors() {
		return loadError;
	}
	
	public static void sendBungeeMessage(Player player, byte[] data) {
		player.sendPluginMessage(getInstance(), "Veranda", data);
	}
	
	public static void whitelistCommand(String name, Rank rank) {
		ChatListener.whitelistCommand(name, rank);
	}
	
	public static void disableCloudInitialization() {
		initializeCloudClient = false;
	}

	public static void shutdown() {
		Bukkit.getScheduler().scheduleSyncDelayedTask(getInstance(), new Runnable() {
			
			@Override
			public void run() {
				Bukkit.shutdown();
			}
		});;
	}
	
	public static GameCloudClient getCloudClient() {
		return cloudClient;
	}

}
