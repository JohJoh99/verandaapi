package eu.veranda.api.bukkit.map;

import java.io.File;
import java.util.ArrayList;
import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import eu.veranda.api.APIUtil;
import eu.veranda.api.VerandaAPI;

public class Map implements Comparable<Map> {
	
	private final String name;
	private final String gamemode;
	private final String map_type;
	private final String author;
	private final String description;
	private final File location;
	private String extraTags;
	private JsonElement extraTagsElement;
	
	protected Map(String name, String gamemode, String map_type, String author, String description, File location, String extraTags) {
		this.name = name;
		this.gamemode = gamemode;
		this.map_type = map_type;
		this.author = author;
		this.description = description;
		this.location = location;
		this.extraTags = extraTags;
	}
	
	public String getExtraTags() {
		return extraTags;
	}
	
	public JsonElement getExtraTagsElement() {
		if(extraTags == null) return null;
		if(extraTagsElement == null) {
			parseExtraTags();
		}
		
		return extraTagsElement;
	}
	
	public String getStringFromElement(String... jsonPath) {
		getExtraTagsElement();
		if(extraTagsElement == null) return null;
		if(jsonPath == null) return null;
		
		JsonElement current = extraTagsElement;
		
		for(String s : jsonPath) {
			if(current.isJsonObject()) {
				current = current.getAsJsonObject().get(s);
			}
			else if(current.isJsonArray()) {
				current = current.getAsJsonArray().get(Integer.parseInt(s));
			}
		}
		
		return current.getAsString();
	}
	
	public int getIntFromElement(String... jsonPath) {
		String integer = getStringFromElement(jsonPath);
		if(APIUtil.isInteger(integer)) {
			return Integer.parseInt(integer);
		}
		else {
			VerandaAPI.log(Level.SEVERE, VerandaAPI.API_PREFIX, "A plugin tried to get a string as int! Is the configuration correct?");
			return -1;
		}
	}
	
	public Vector getVectorFromElement(String... jsonPath) {
		getExtraTagsElement();
		if(extraTagsElement == null) return null;
		if(jsonPath == null) return null;
		
		JsonElement current = extraTagsElement;
		for(String s : jsonPath) {
			if(current.isJsonObject()) {
				current = current.getAsJsonObject().get(s);
			}
			else if(current.isJsonArray()) {
				current = current.getAsJsonArray().get(Integer.parseInt(s));
			}
		}
		
		JsonObject obj = current.getAsJsonObject();
		return parseVector(obj);
	}
	
	public Location getLocationFromElement(World world, String... jsonPath) {
		getExtraTagsElement();
		if(extraTagsElement == null) return null;
		if(jsonPath == null) return null;
		
		JsonElement current = extraTagsElement;
		for(String s : jsonPath) {
			if(current.isJsonObject()) {
				current = current.getAsJsonObject().get(s);
			}
			else if(current.isJsonArray()) {
				current = current.getAsJsonArray().get(Integer.parseInt(s));
			}
		}
		
		JsonObject obj = current.getAsJsonObject();
		return parseLocation(world, obj);
	}
	
	public Location[] getLocationsFromElementArray(World world, String... jsonPath) {
		getExtraTagsElement();
		if(extraTagsElement == null) return null;
		if(jsonPath == null) return null;
		
		JsonElement current = extraTagsElement;
		for(String s : jsonPath) {
			if(current.isJsonObject()) {
				current = current.getAsJsonObject().get(s);
			}
			else if(current.isJsonArray()) {
				current = current.getAsJsonArray().get(Integer.parseInt(s));
			}
		}
		
		ArrayList<Location> locations = new ArrayList<Location>();
		JsonArray array = current.getAsJsonArray();
		for(int i = 0; i < array.size(); i++) {
			locations.add(parseLocation(world, array.get(i).getAsJsonObject()));
		}
		
		return locations.toArray(new Location[locations.size()]);
	}
	
	private Location parseLocation(World world, JsonObject obj) {
		Vector vec = parseVector(obj);
		float yaw = 0;
		float pitch = 0;
		if(obj.has("yaw")) {
			yaw = obj.get("yaw").getAsFloat();
		}
		if(obj.has("pitch")) {
			pitch = obj.get("pitch").getAsFloat();
		}
		return new Location(world, vec.getX(), vec.getY(), vec.getZ(), yaw, pitch);
	}
	
	private Vector parseVector(JsonObject obj) {
		double x = obj.get("x").getAsDouble();
		double y = obj.get("y").getAsDouble();
		double z = obj.get("z").getAsDouble();
		
		return new Vector(x, y, z);
	}
	
	private void parseExtraTags() {
		JsonParser parser = new JsonParser();
		try {
			extraTagsElement = parser.parse(extraTags);
		}
		catch(JsonSyntaxException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e);
		}
	}
	
	public String getName() {
		return name;
	}
	
	public String getGameMode() {
		return gamemode;
	}
	
	public String getMapType() {
		return map_type;
	}
	
	public String getAuthor() {
		return author;
	}
	
	public String getDescription() {
		return description;
	}
	
	public File getLocation() {
		return location;
	}

	@Override
	public int compareTo(Map o) {
		return name.compareTo(o.getName());
	}


}
