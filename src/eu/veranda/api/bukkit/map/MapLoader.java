package eu.veranda.api.bukkit.map;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import eu.veranda.api.VerandaAPI;
import eu.veranda.api.sql.MySQLConnectionUtils;

public class MapLoader {
	
	public static boolean incrementTimesPlayed(Map map) {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			PreparedStatement st = con.prepareStatement("UPDATE `" + VerandaAPI.TABLE_MAPS + "` SET `" + VerandaAPI.COLUMN_TIMES_PLAYED + "`=`" + VerandaAPI.COLUMN_TIMES_PLAYED + "`+1 WHERE `name`='" + map.getName() + "' AND `gamemode`='" + map.getGameMode() + "'");
			st.execute();
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method MapLoader.incrementTimesPlayed() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	private final String gamemode;
	private Map gameLobbyMap;
	private ArrayList<Map> gameMaps = new ArrayList<Map>();
	private Map serverLobbyMap;
	private Map deathmatchMap;
	
	public MapLoader(String gamemode) {
		this.gamemode = gamemode;
	}
	
	public String getGameMode() {
		return gamemode;
	}
	
	public boolean load() {
		Connection con = null;
		try {
			con = MySQLConnectionUtils.getNewConnection();
			return load(con);
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method MapLoader.load() occurred an Error");
			return false;
		}
		finally {
			MySQLConnectionUtils.closeConnection(con);
		}
	}
	
	public boolean load(Connection con) {
		try {
			PreparedStatement query = con.prepareStatement("SELECT * FROM `" + VerandaAPI.TABLE_MAPS + "` WHERE `" + VerandaAPI.COLUMN_GAMEMODE + "`='" + gamemode.toUpperCase() + "'");
			ResultSet rs = query.executeQuery();
			
			while(rs.next()) {
				String mapType = rs.getString(VerandaAPI.COLUMN_MAP_TYPE);
				String mapName = rs.getString(VerandaAPI.COLUMN_NAME);
				File loc = new File(rs.getString(VerandaAPI.COLUMN_LOCATION));
				if(!loc.exists() || !loc.isDirectory()) {
					VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "Location of Map \"" + mapName + "\" not found! Ignoring map.");
					continue;
				}
				Map map = new Map(mapName, rs.getString(VerandaAPI.COLUMN_GAMEMODE), mapType, rs.getString(VerandaAPI.COLUMN_AUTHOR), rs.getString(VerandaAPI.COLUMN_DESCRIPTION), loc, rs.getString(VerandaAPI.COLUMN_EXTRA_TAGS));
				if(mapType.equalsIgnoreCase("game_lobby")) gameLobbyMap = map;
				else if(mapType.equalsIgnoreCase("game_map")) gameMaps.add(map);
				else if(mapType.equalsIgnoreCase("server_lobby")) serverLobbyMap = map;
				else if(mapType.equalsIgnoreCase("deathmatch")) deathmatchMap = map;
				else {
					VerandaAPI.log(Level.WARNING, VerandaAPI.API_PREFIX, "Unknown Map Type \"" + mapType + "\" found! Ignoring map.");
				}
			}
			
			rs.close();
			return true;
		}
		catch(SQLException e) {
			VerandaAPI.logMinimal(Level.SEVERE, VerandaAPI.API_PREFIX, e, "Method MapLoader.load() occurred an Error");
			return false;
		}
	}
	
	public Map getGameLobbyMap() {
		return gameLobbyMap;
	}
	
	public Map[] getGameMaps() {
		return gameMaps.toArray(new Map[gameMaps.size()]);
	}
	
	public Map getServerLobbyMap() {
		return serverLobbyMap;
	}
	
	public Map getDeathMatchMap() {
		return deathmatchMap;
	}

}
