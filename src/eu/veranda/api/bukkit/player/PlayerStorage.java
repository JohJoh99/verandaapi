package eu.veranda.api.bukkit.player;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;

import eu.veranda.api.APIUtil;

public class PlayerStorage {
	
	private static HashMap<UUID, VerandaPlayer> players = new HashMap<UUID, VerandaPlayer>();
	private static HashMap<UUID, VerandaPlayer> spectators = new HashMap<UUID, VerandaPlayer>();
	
	public static void registerPlayer(VerandaPlayer player) { players.put(player.getUniqueId(), player); }
	public static void unregisterPlayer(UUID playerUUID) { players.remove(playerUUID); }
	public static void unregisterPlayer(Player player) { unregisterPlayer(player.getUniqueId()); }
	public static boolean isPlayerRegistered(UUID playerUUID) { return players.containsKey(playerUUID); }
	public static VerandaPlayer getRegisteredPlayer(UUID playerUUID) { return players.get(playerUUID); }
	public static VerandaPlayer getRegisteredPlayer(Player player) { return getRegisteredPlayer(player.getUniqueId()); }
	public static boolean isPlayerRegistered(Player player) { return isPlayerRegistered(player.getUniqueId()); }
	public static void registerSpectator(VerandaPlayer player) { spectators.put(player.getUniqueId(), player); }
	public static void unregisterSpectator(UUID playerUUID) { spectators.remove(playerUUID); }
	public static void unregisterSpectator(Player player) { unregisterSpectator(player.getUniqueId()); }
	public static boolean isSpectatorRegistered(UUID playerUUID) { return spectators.containsKey(playerUUID); }
	public static VerandaPlayer getRegisteredSpectator(UUID playerUUID) { return spectators.get(playerUUID); }
	public static VerandaPlayer getRegisteredSpectator(Player player) { return getRegisteredSpectator(player.getUniqueId()); }
	public static boolean isSpectatorRegistered(Player player) { return isSpectatorRegistered(player.getUniqueId()); }
	public static VerandaPlayer getRegistered(UUID playerUUID) { if(players.containsKey(playerUUID)) return players.get(playerUUID); else if(spectators.containsKey(playerUUID)) return spectators.get(playerUUID); else return null; }
	public static VerandaPlayer getRegistered(Player player) { return getRegistered(player.getUniqueId()); }
	public static boolean isRegistered(UUID playerUUID) { return getRegistered(playerUUID) != null; }
	public static boolean isRegistered(Player player) { return isRegistered(player.getUniqueId()); }
	public static void unregister(UUID playerUUID) { if(players.containsKey(playerUUID)) players.remove(playerUUID); else if(spectators.containsKey(playerUUID)) spectators.remove(playerUUID); }
	public static void unregister(Player player) { unregister(player.getUniqueId()); }
	public static Collection<VerandaPlayer> getPlayers() { return APIUtil.copy(players.values()); }
	public static Collection<VerandaPlayer> getSpectators() { return APIUtil.copy(spectators.values()); }
	public static Collection<VerandaPlayer> getRegistered() { return APIUtil.merge(players.values(), spectators.values()); }

}
