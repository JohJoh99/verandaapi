package eu.veranda.api.bukkit.cmd;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

import eu.veranda.api.VerandaAPI;
import eu.veranda.api.player.Rank;

public class RAMCommand extends VerandaCommand {

	public RAMCommand(Plugin plugin) {
		super("ram", Rank.DEVELOPER, CommandType.BOTH);
	}
	
	@Override
	public void exec(CommandSender cs, String[] args) {
		int MB = 1048576;
		Runtime runtime = Runtime.getRuntime();
		
		long free = runtime.freeMemory() / MB;
		long allocated = runtime.totalMemory() / MB;
		
		cs.sendMessage(VerandaAPI.VERANDA_PREFIX + ChatColor.GRAY + "Used RAM" + ChatColor.DARK_GRAY + ": " + ChatColor.YELLOW + (allocated-free) + "MB");
	}

}
