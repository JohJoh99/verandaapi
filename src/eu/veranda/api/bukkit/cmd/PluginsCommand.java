package eu.veranda.api.bukkit.cmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import eu.veranda.api.bukkit.player.PlayerStorage;
import eu.veranda.api.bukkit.player.VerandaPlayer;
import eu.veranda.api.player.Rank;

public class PluginsCommand extends VerandaCommand {

	public PluginsCommand(Plugin plugin) {
		super("plugins");
	}

	@Override
	public void exec(CommandSender cs, String[] args) {
		if(cs instanceof Player) {
			VerandaPlayer player = PlayerStorage.getRegistered((Player)cs);
			if(player.hasRank(Rank.DEVELOPER)) {
				Plugin[] pls = Bukkit.getPluginManager().getPlugins();
				String plugins = "";
				int i = 0;
				for(Plugin plugin : pls) {
					if(plugin.isEnabled()) plugins += ChatColor.GREEN; else plugins += ChatColor.RED;
					plugins += plugin.getName();
					i++;
					if(i < pls.length) {
						plugins += ChatColor.GRAY + ", ";
					}
				}
				player.sendMessage(ChatColor.AQUA + "Plugins" + ChatColor.DARK_GRAY + " \u00BB " + plugins);
			}
			else {
				player.sendMessage("Plugins (10): " + ChatColor.GREEN + "Essentials" + ChatColor.WHITE + ", " + ChatColor.GREEN + "PermissionsEx" + ChatColor.WHITE + ", "
						+ ChatColor.GREEN + "WorldEdit" + ChatColor.WHITE + ", " + ChatColor.GREEN + "WorldGuard" + ChatColor.WHITE + ", " + ChatColor.GREEN + "NoCheatPlus"
						+ ChatColor.WHITE + ", " + ChatColor.GREEN + "ServerSigns" + ChatColor.WHITE + ", " + ChatColor.GREEN + "ClearLagg" + ChatColor.WHITE + ", "
						+ ChatColor.GREEN + "Multiverse" + ChatColor.WHITE + ", "	+ ChatColor.GREEN + "ProtocolLib" + ChatColor.WHITE + ", " + ChatColor.GREEN + "TopPvP");
			}
		}
		else {
			Plugin[] pls = Bukkit.getPluginManager().getPlugins();
			String plugins = "";
			int i = 0;
			for(Plugin plugin : pls) {
				if(plugin.isEnabled()) plugins += ChatColor.GREEN; else plugins += ChatColor.RED;
				plugins += plugin.getName();
				i++;
				if(i < pls.length) {
					plugins += ChatColor.GRAY + ", ";
				}
			}
			cs.sendMessage(ChatColor.AQUA + "Plugins" + ChatColor.DARK_GRAY + " \u00BB " + plugins);
		}
	}

}
