package eu.veranda.api.bukkit.cmd;

import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;

import eu.veranda.api.VerandaAPI;
import eu.veranda.api.bukkit.VerandaAPIPlugin;
import eu.veranda.api.bukkit.player.PlayerStorage;
import eu.veranda.api.bukkit.player.VerandaPlayer;
import eu.veranda.api.lang.APILangKeys;
import eu.veranda.api.lang.LanguageUtils;
import eu.veranda.api.player.Rank;

public abstract class VerandaCommand implements CommandExecutor {
	
	public static enum CommandType { PLAYER_ONLY, CONSOLE_ONLY, BOTH }
	
	private final String name;
	private final Rank minRank;
	private final CommandType type;
	
	public VerandaCommand(String name) {
		this.name = name;
		this.minRank = Rank.USER;
		this.type = CommandType.BOTH;
		registerCommand();
	}
	
	public VerandaCommand(String name, Rank minRank) {
		this.name = name;
		this.minRank = minRank;
		this.type = CommandType.BOTH;
		registerCommand();
	}
	
	public VerandaCommand(String name, CommandType type) {
		this.name = name;
		this.minRank = Rank.USER;
		this.type = type;
		registerCommand();
	}
	
	public VerandaCommand(String name, Rank minRank, CommandType type) {
		this.name = name;
		this.minRank = minRank;
		this.type = type;
		registerCommand();
	}
	
	private void registerCommand() {
		PluginCommand command = Bukkit.getPluginCommand(name);
		if(command != null) {
			VerandaAPI.log(Level.INFO, VerandaAPI.API_PREFIX, "Registering Command: " + name);
			command.setExecutor(this);
			VerandaAPIPlugin.whitelistCommand(name, minRank);
			for(String alias : command.getAliases()) {
				VerandaAPIPlugin.whitelistCommand(alias, minRank);
			}
		}
	}
	
	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase(name)) {
			if(cs instanceof Player && type != CommandType.CONSOLE_ONLY) {
				if(minRank != Rank.USER) {
					VerandaPlayer p = PlayerStorage.getRegistered((Player) cs);
					if(p != null) {
						if(p.hasRank(minRank)) {
							try {
								exec(cs, args);
							}
							catch(Throwable e) {
								handleException(e);
							}
						}
						else {
							p.sendTranslation(APILangKeys.COMMAND_NO_PERMISSION);
						}
					}
					else {
						cs.sendMessage(LanguageUtils.getTranslation(APILangKeys.COMMAND_NO_PERMISSION, 0));
					}
				}
				else {
					try {
						exec(cs, args);
					}
					catch(Throwable e) {
						handleException(e);
					}
				}
			}
			else if(cs instanceof ConsoleCommandSender && type != CommandType.PLAYER_ONLY) {
				try {
					exec(cs, args);
				}
				catch(Throwable e) {
					handleException(e);
				}
			}
			else {
				if(cs instanceof Player && PlayerStorage.getRegistered((Player) cs) != null) {
					PlayerStorage.getRegistered((Player) cs).sendTranslation(APILangKeys.COMMAND_NO_EXECUTION_POSSIBLE);
				}
				else {
					cs.sendMessage(LanguageUtils.getTranslation(APILangKeys.COMMAND_NO_EXECUTION_POSSIBLE, 0));
				}
			}
		}
		return true;
	}
	
	private void handleException(Throwable t) {
//		VerandaAPI.log(name.toUpperCase() + " command caused an Exception", VerandaAPI.API_PREFIX, t, Thread.currentThread());
//		String title = "Uncaught command error on " + VerandaAPI.SERVER_NAME;
//		String text = "Command Name: " + name + "\n";
//		text += "Exception: " + t.getClass().getName() + "\n";
//		text += "Message: " + t.getMessage() + "\n";
//		text += " \n";
//		text += "Stack Trace:\n";
//		for(StackTraceElement ste : t.getStackTrace()) {
//			text += ste.toString() + "\n";
//		}
//		text.substring(0, text.length() - 2);
	}
	
	public abstract void exec(CommandSender cs, String[] args);

}
