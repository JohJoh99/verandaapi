package eu.veranda.api.sql;

import java.util.ArrayList;

public class TableCreator {
	
	private String tableName;
	private String ifNotExists = "";
	private ArrayList<String> cols = new ArrayList<String>();
	private String primary = "";
	private String unique = "";
	private String engine = "MyISAM";
	private String defaultCharset = "utf8";
	
	public TableCreator(String tableName) {
		this.tableName = tableName;
	}
	
	public TableCreator ifNotExists() {
		ifNotExists = " IF NOT EXISTS";
		return this;
	}
	
	/**
	 * 
	 * @param columnName
	 * @param type
	 * @param notNull
	 * @param defaultValue Can be null
	 * @return
	 */
	public TableCreator column(String columnName, String type, boolean notNull, String defaultValue) {
		if(defaultValue != null) defaultValue = defaultValue.trim();
		String strNotNull = notNull ? "NOT NULL" : "NULL";
		String strDefVal = "";
		if(defaultValue != null) {
			if(defaultValue.equalsIgnoreCase("AUTO_INCREMENT")) {
				strDefVal = " " + defaultValue;
			}
			else if(defaultValue.equalsIgnoreCase("CURRENT_TIMESTAMP")) {
				strDefVal = " DEFAULT " + defaultValue;
			}
			else {
				strDefVal = " DEFAULT '" + defaultValue +  "'";
			}
		}
		this.cols.add("`" + columnName + "` " + type + " " + strNotNull + strDefVal);
		return this;
	}
	
	public TableCreator unique(String unique) {
		this.unique = ", UNIQUE KEY `" + unique + "` (`" + unique + "`)";
		return this;
	}
	
	public TableCreator primary(String primary) {
		this.primary = ", PRIMARY KEY (`" + primary + "`)";
		return this;
	}
	
	public TableCreator engine(String engine) {
		this.engine = engine;
		return this;
	}
	
	public TableCreator defaultCharset(String defaultCharset) {
		this.defaultCharset = defaultCharset;
		return this;
	}
	
	public String getStatement() {
		String colStr = "";
		for(String col : cols) colStr += col + ", ";
		if(colStr.length() > 0) colStr = colStr.substring(0, colStr.length() - 2);
		
		return "CREATE TABLE" + ifNotExists + " `" + tableName + "` (" + colStr + primary + unique + ") ENGINE=" + engine + " DEFAULT CHARSET=" + defaultCharset;
	}

}
