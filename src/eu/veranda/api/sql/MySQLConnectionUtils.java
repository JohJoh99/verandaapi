package eu.veranda.api.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLConnectionUtils {
	
	private static String host;
	private static int port;
	private static String username;
	private static String password;
	private static String database;
	
	public static boolean reloadCfgFromProperties() {
		host = System.getProperty("mysql-host");
		try {
			port = Integer.parseInt(System.getProperty("mysql-port"));
		}
		catch(NumberFormatException e) {
			port = 3306;
		}
		username = System.getProperty("mysql-username");
		password = System.getProperty("mysql-password");
		database = System.getProperty("mysql-database");
		
		try {
			port = Integer.parseInt(System.getProperty("mysql-port"));
		}
		catch(NumberFormatException e) {
			port = 3306;
		}
		
		return host != null && username != null && password != null && database != null;
	}
	
	public static Connection getNewConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
	}
	
	public static Connection getNewConnectionOrNull() {
		try {
			return getNewConnection();
		}
		catch(SQLException e) {
			return null;
		}
	}
	
	public static String getHost() {
		return host;
	}
	
	public static boolean isConnectionValid(Connection con) throws SQLException {
		if(con != null) {
			if(!con.isClosed()) {
				if(con.isValid(1)) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	
	public static boolean closeResources(ResultSet rs, PreparedStatement st) {
		try {
			if(rs != null) {
				rs.close();
			}
			if(st != null) {
				st.close();
			}	
			return true;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean closeConnection(Connection con) {
		try {
			if(con != null) {
				con.close();
			}
			con = null;
			return true;
		}
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean closeConnection(ResultSet rs) {
		try {
			return closeConnection(rs.getStatement().getConnection());
		}
		catch(SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

}
