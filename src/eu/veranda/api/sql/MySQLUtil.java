package eu.veranda.api.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQLUtil {
	
	public static void updateInt(String table, String col, int val, String where, String whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + val + " WHERE `" + where + "`='" + whereVal + "'").execute();
	}
	
	public static void updateInt(String table, String col, int val, String where, int whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + val + " WHERE `" + where + "`=" + whereVal + "").execute();
	}
	
	public static void updateDouble(String table, String col, double val, String where, String whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + val + " WHERE `" + where + "`='" + whereVal + "'").execute();
	}
	
	public static void updateDouble(String table, String col, double val, String where, int whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + val + " WHERE `" + where + "`=" + whereVal + "").execute();
	}
	
	public static void updateString(String table, String col, String val, String where, String whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`='" + val + "' WHERE `" + where + "`='" + whereVal + "'").execute();
	}
	
	public static void updateString(String table, String col, String val, String where, int whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`='" + val + "' WHERE `" + where + "`=" + whereVal + "").execute();
	}
	
	public static void updateBoolean(String table, String col, boolean val, String where, String whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + (val ? 1 : 0) + " WHERE `" + where + "`='" + whereVal + "'").execute();
	}
	
	public static void updateBoolean(String table, String col, boolean val, String where, int whereVal, Connection con) throws SQLException {
		con.prepareStatement("UPDATE `" + table + "` SET `" + col + "`=" + (val ? 1 : 0) + " WHERE `" + where + "`=" + whereVal + "").execute();
	}
	
	public static ResultSet select(String what, String table, String where, String whereVal, Connection con) throws SQLException {
		return con.prepareStatement("SELECT `" + what + "` FROM `" + table + "` WHERE `" + where + "`='" + whereVal + "'").executeQuery();
	}
	
	public static ResultSet select(String what, String table, String where, int whereVal, Connection con) throws SQLException {
		return con.prepareStatement("SELECT `" + what + "` FROM `" + table + "` WHERE `" + where + "`=" + whereVal + "").executeQuery();
	}
	
	public static ResultSet selectAll(String table, String where, String whereVal, Connection con) throws SQLException {
		return con.prepareStatement("SELECT * FROM `" + table + "` WHERE `" + where + "`='" + whereVal + "'").executeQuery();
	}
	
	public static ResultSet selectAll(String table, String where, int whereVal, Connection con) throws SQLException {
		return con.prepareStatement("SELECT * FROM `" + table + "` WHERE `" + where + "`=" + whereVal + "").executeQuery();
	}

}
