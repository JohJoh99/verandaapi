package eu.veranda.api.sql;

import static eu.veranda.api.VerandaAPI.COLUMN_ACHIEVEMENTS;
import static eu.veranda.api.VerandaAPI.COLUMN_ALLOW_PARTY_INVITE;
import static eu.veranda.api.VerandaAPI.COLUMN_AUTHOR;
import static eu.veranda.api.VerandaAPI.COLUMN_AUTONICK;
import static eu.veranda.api.VerandaAPI.COLUMN_BLOCK_FACE;
import static eu.veranda.api.VerandaAPI.COLUMN_COINS;
import static eu.veranda.api.VerandaAPI.COLUMN_CURRENT_SERVER;
import static eu.veranda.api.VerandaAPI.COLUMN_DESCRIPTION;
import static eu.veranda.api.VerandaAPI.COLUMN_ENGLISH;
import static eu.veranda.api.VerandaAPI.COLUMN_EXTRA_TAGS;
import static eu.veranda.api.VerandaAPI.COLUMN_FIRST_LOGIN;
import static eu.veranda.api.VerandaAPI.COLUMN_FRIENDS;
import static eu.veranda.api.VerandaAPI.COLUMN_FRIEND_JOIN_MESSAGES;
import static eu.veranda.api.VerandaAPI.COLUMN_FRIEND_JUMP;
import static eu.veranda.api.VerandaAPI.COLUMN_FRIEND_REQUESTS;
import static eu.veranda.api.VerandaAPI.COLUMN_GAMEMODE;
import static eu.veranda.api.VerandaAPI.COLUMN_GERMAN;
import static eu.veranda.api.VerandaAPI.COLUMN_GOTTEN_REQUESTS;
import static eu.veranda.api.VerandaAPI.COLUMN_ID;
import static eu.veranda.api.VerandaAPI.COLUMN_KEY;
import static eu.veranda.api.VerandaAPI.COLUMN_LANGUAGE;
import static eu.veranda.api.VerandaAPI.COLUMN_LAST_LOGIN;
import static eu.veranda.api.VerandaAPI.COLUMN_LAST_LOGOUT;
import static eu.veranda.api.VerandaAPI.COLUMN_LOCATION;
import static eu.veranda.api.VerandaAPI.COLUMN_MAIN_INVENTORY;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_AUTHOR;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_DESCRIPTION;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_EXTRA_TAGS;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_GAMEMODE;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_GENERATOR;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_LOADED;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_NAME;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_SPAWN_PITCH;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_SPAWN_X;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_SPAWN_Y;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_SPAWN_YAW;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_SPAWN_Z;
import static eu.veranda.api.VerandaAPI.COLUMN_MAP_TYPE;
import static eu.veranda.api.VerandaAPI.COLUMN_NAME;
import static eu.veranda.api.VerandaAPI.COLUMN_ONLINE;
import static eu.veranda.api.VerandaAPI.COLUMN_PLAYER_NAME;
import static eu.veranda.api.VerandaAPI.COLUMN_PLAYER_UUID;
import static eu.veranda.api.VerandaAPI.COLUMN_PLAYER_XP;
import static eu.veranda.api.VerandaAPI.COLUMN_PRIVATE_MESSAGES;
import static eu.veranda.api.VerandaAPI.COLUMN_RANK;
import static eu.veranda.api.VerandaAPI.COLUMN_SENT_REQUESTS;
import static eu.veranda.api.VerandaAPI.COLUMN_SERVER_NAME;
import static eu.veranda.api.VerandaAPI.COLUMN_SHOW_ONLY_YT_AND_TEAM;
import static eu.veranda.api.VerandaAPI.COLUMN_SHOW_STATS;
import static eu.veranda.api.VerandaAPI.COLUMN_SILENT_HUB;
import static eu.veranda.api.VerandaAPI.COLUMN_STORAGE_INVENTORY;
import static eu.veranda.api.VerandaAPI.COLUMN_STORAGE_INVENTORY_SIZE;
import static eu.veranda.api.VerandaAPI.COLUMN_TEAM_MESSAGES;
import static eu.veranda.api.VerandaAPI.COLUMN_TIMES_PLAYED;
import static eu.veranda.api.VerandaAPI.COLUMN_VALUE;
import static eu.veranda.api.VerandaAPI.COLUMN_X;
import static eu.veranda.api.VerandaAPI.COLUMN_Y;
import static eu.veranda.api.VerandaAPI.COLUMN_Z;
import static eu.veranda.api.VerandaAPI.TABLE_BUILD_MAPS;
import static eu.veranda.api.VerandaAPI.TABLE_DEFENDERS_INVENTORYS;
import static eu.veranda.api.VerandaAPI.TABLE_FRIENDS;
import static eu.veranda.api.VerandaAPI.TABLE_LANG;
import static eu.veranda.api.VerandaAPI.TABLE_MAPS;
import static eu.veranda.api.VerandaAPI.TABLE_MOTD;
import static eu.veranda.api.VerandaAPI.TABLE_SIGNS_LOBBY;
import static eu.veranda.api.VerandaAPI.TABLE_USERS;

import java.sql.Connection;
import java.sql.SQLException;

public class CreateTables {
	
	public static void createAllTables(Connection con) throws SQLException {
		createUsersTable(con);
		createMapTable(con);
		createMotdTable(con);
		createLangTable(con);
		createSignTable(con);
		createFriendsTable(con);
		createDefendersInventoryTable(con);
		createBuildMapsTable(con);
	}
	
	public static void createUsersTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_USERS).ifNotExists().primary(COLUMN_ID).unique(COLUMN_PLAYER_UUID).engine("MyISAM").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_PLAYER_UUID, "char(36)", true, null);
		tc.column(COLUMN_PLAYER_NAME, "varchar(16)", true, null);
		tc.column(COLUMN_COINS, "int(11)", true, "0");
		tc.column(COLUMN_LANGUAGE, "int(11)", true, "0");
		tc.column(COLUMN_RANK, "int(11)", true, "0");
		tc.column(COLUMN_PLAYER_XP, "int(11)", true, "0");
		tc.column(COLUMN_ONLINE, "tinyint(1)", true, "0");
		tc.column(COLUMN_CURRENT_SERVER, "varchar(255)", false, null);
		tc.column(COLUMN_FIRST_LOGIN, "datetime", true, "0000-00-00 00:00:00");
		tc.column(COLUMN_LAST_LOGIN, "datetime", true, "0000-00-00 00:00:00");
		tc.column(COLUMN_LAST_LOGOUT, "datetime", true, "0000-00-00 00:00:00");
		tc.column(COLUMN_ACHIEVEMENTS, "text", false, null);
		tc.column(COLUMN_AUTONICK, "tinyint(1)", true, "0");
		tc.column(COLUMN_SILENT_HUB, "tinyint(1)", true, "0");
		tc.column(COLUMN_SHOW_ONLY_YT_AND_TEAM, "tinyint(1)", true, "0");
		tc.column(COLUMN_ALLOW_PARTY_INVITE, "tinyint(1)", true, "1");
		tc.column(COLUMN_SHOW_STATS, "tinyint(1)", true, "0");
		tc.column(COLUMN_PRIVATE_MESSAGES, "tinyint(1)", true, "1");
		tc.column(COLUMN_TEAM_MESSAGES, "tinyint(1)", true, "1");
		
		con.prepareStatement(tc.getStatement()).execute();
	}
	
	public static void createMapTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_MAPS).ifNotExists().primary(COLUMN_ID).unique(COLUMN_NAME).engine("InnoDB").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_GAMEMODE, "varchar(255)", true, null);
		tc.column(COLUMN_MAP_TYPE, "enum('GAME_LOBBY','GAME_MAP','SERVER_LOBBY','DEATHMATCH')", true, null);
		tc.column(COLUMN_NAME, "varchar(255)", true, null);
		tc.column(COLUMN_AUTHOR, "varchar(255)", true, null);
		tc.column(COLUMN_DESCRIPTION, "varchar(255)", true, null);
		tc.column(COLUMN_LOCATION, "varchar(255)", true, null);
		tc.column(COLUMN_EXTRA_TAGS, "text", true, null);
		tc.column(COLUMN_TIMES_PLAYED, "int(11)", true, "0");
		
		con.prepareStatement(tc.getStatement()).execute();
	}

	public static void createMotdTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_MOTD).ifNotExists().primary(COLUMN_ID).unique(COLUMN_NAME).engine("InnoDB").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_NAME, "varchar(255)", true, null);
		tc.column(COLUMN_VALUE, "varchar(255)", true, null);
		
		con.prepareStatement(tc.getStatement()).execute();
	}

	public static void createLangTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_LANG).ifNotExists().primary(COLUMN_ID).unique(COLUMN_KEY).engine("InnoDB").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_KEY, "varchar(255)", true, null);
		tc.column(COLUMN_GERMAN, "text", true, null);
		tc.column(COLUMN_ENGLISH, "text", true, null);
		
		con.prepareStatement(tc.getStatement()).execute();
	}
	
	public static void createSignTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_SIGNS_LOBBY).ifNotExists().primary(COLUMN_ID).engine("InnoDB").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_SERVER_NAME, "varchar(16)", true, null);
		tc.column(COLUMN_X, "int(11)", true, null);
		tc.column(COLUMN_Y, "int(11)", true, null);
		tc.column(COLUMN_Z, "int(11)", true, null);
		tc.column(COLUMN_BLOCK_FACE, "varchar(255)", true, null);
		
		con.prepareStatement(tc.getStatement()).execute();
	}
	
	public static void createFriendsTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_FRIENDS).ifNotExists().primary(COLUMN_ID).unique(COLUMN_PLAYER_UUID).engine("MyISAM").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_PLAYER_UUID, "char(36)", true, null);
		tc.column(COLUMN_FRIEND_REQUESTS, "tinyint(1)", true, "1");
		tc.column(COLUMN_FRIEND_JOIN_MESSAGES, "tinyint(1)", true, "1");
		tc.column(COLUMN_FRIEND_JUMP, "tinyint(1)", true, "1");
		tc.column(COLUMN_FRIENDS, "varchar(2047)", false, null);
		tc.column(COLUMN_SENT_REQUESTS, "varchar(2047)", false, null);
		tc.column(COLUMN_GOTTEN_REQUESTS, "varchar(2047)", false, null);
		
		con.prepareStatement(tc.getStatement()).execute();
	}
	
	public static void createDefendersInventoryTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_DEFENDERS_INVENTORYS).ifNotExists().primary(COLUMN_ID).unique(COLUMN_PLAYER_UUID).engine("MyISAM").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_PLAYER_UUID, "char(36)", true, null);
		tc.column(COLUMN_MAIN_INVENTORY, "text", false, null);
		tc.column(COLUMN_STORAGE_INVENTORY, "text", false, null);
		tc.column(COLUMN_STORAGE_INVENTORY_SIZE, "int(11)", true, String.valueOf(9));
		
		con.prepareStatement(tc.getStatement()).execute();
	}
	
	public static void createBuildMapsTable(Connection con) throws SQLException {
		TableCreator tc = new TableCreator(TABLE_BUILD_MAPS).ifNotExists().primary(COLUMN_ID).unique(COLUMN_MAP_NAME).engine("MyISAM").defaultCharset("utf8");
		tc.column(COLUMN_ID, "int(11)", true, "AUTO_INCREMENT");
		tc.column(COLUMN_MAP_NAME, "char(36)", true, null);
		tc.column(COLUMN_MAP_SPAWN_X, "double", true, "0.0");
		tc.column(COLUMN_MAP_SPAWN_Y, "double", true, "64.0");
		tc.column(COLUMN_MAP_SPAWN_Z, "double", true, "0.0");
		tc.column(COLUMN_MAP_SPAWN_YAW, "float", true, "0.0");
		tc.column(COLUMN_MAP_SPAWN_PITCH, "float", true, "0.0");
		tc.column(COLUMN_MAP_LOADED, "tinyint(1)", true, "1");
		tc.column(COLUMN_MAP_GENERATOR, "int(11)", true, "0");
		tc.column(COLUMN_MAP_GAMEMODE, "varchar(255)", false, null);
		tc.column(COLUMN_MAP_TYPE, "enum('GAME_LOBBY','GAME_MAP','SERVER_LOBBY','DEATHMATCH')", false, null);
		tc.column(COLUMN_MAP_AUTHOR, "varchar(255)", false, null);
		tc.column(COLUMN_MAP_DESCRIPTION, "varchar(255)", false, null);
		tc.column(COLUMN_MAP_EXTRA_TAGS, "text", false, null);
		
		con.prepareStatement(tc.getStatement()).execute();
	}

}
